package br.ufmg.dcc.llp.ovisa.project.model.dao.impl;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IRepositoryDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.IRepositoryTypeDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Repository;
import br.ufmg.dcc.llp.ovisa.project.model.domain.RepositoryType;
import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;
import javax.persistence.Query;

public class RepositoryDAO extends GenericDAO<Repository> implements IRepositoryDAO {

    @Override
    protected void prePersist(Repository repository) {
        IRepositoryTypeDAO rtdao = new RepositoryTypeDAO();
        RepositoryType rt = rtdao.consultarPorNome(repository.getRepositoryType().getName());
        if (rt == null)
            rt = rtdao.adicionar(repository.getRepositoryType());
        repository.setRepositoryType(rt);
    }
    
    @Override
    public Repository consultarPorURL(String repositoryURL) {
        Query qry = em.createNamedQuery("Repository.findByUrlAddress", Repository.class);
        qry.setParameter("urlAddress", repositoryURL);
        
        if (qry.getResultList().isEmpty())
            return null;
               
        return (Repository)qry.getSingleResult();
    }
}
