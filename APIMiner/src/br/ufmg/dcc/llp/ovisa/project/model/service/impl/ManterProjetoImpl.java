package br.ufmg.dcc.llp.ovisa.project.model.service.impl;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.ProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.project.model.service.IManterProjeto;
import java.util.List;

public class ManterProjetoImpl implements IManterProjeto {

    private IProjectDAO pdao = new ProjectDAO();
    
    public ManterProjetoImpl() {
    }

    @Override
    public Project adicionar(Project projeto) {
        return this.pdao.adicionar(projeto);
    }

    @Override
    public Project consultarPorId(Long projectId) {
        return this.pdao.consultarPorId(projectId);
    }

    @Override
    public Project consultarPorNome(String projectNome) {
        return this.pdao.consultarPorNome(projectNome);
    }

    @Override
    public List<Project> consultarTodos() {
        return this.pdao.consultarTodos();
    }


}
