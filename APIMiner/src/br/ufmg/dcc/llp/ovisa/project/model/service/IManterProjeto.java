package br.ufmg.dcc.llp.ovisa.project.model.service;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import java.util.List;

public interface IManterProjeto {
    public Project adicionar(Project projeto);
    public Project consultarPorId(Long projectId);
    public Project consultarPorNome(String projectNome);
    public List<Project> consultarTodos();
}
