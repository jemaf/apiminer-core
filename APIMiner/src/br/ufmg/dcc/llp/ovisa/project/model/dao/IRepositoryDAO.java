package br.ufmg.dcc.llp.ovisa.project.model.dao;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Repository;

public interface IRepositoryDAO {
    public Repository adicionar(Repository repository);
    public Repository atualizar(Repository repository);
    public Repository remover(Repository repository);
    public Repository consultarPorId(Long repositoryId);
    public Repository consultarPorURL(String repositoryURL);
}
