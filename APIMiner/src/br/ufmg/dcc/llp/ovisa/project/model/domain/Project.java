package br.ufmg.dcc.llp.ovisa.project.model.domain;

import br.ufmg.dcc.llp.ovisa.repository.model.domain.Revision;
import br.ufmg.dcc.llp.util.persistence.jpa.IGenericEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Project")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
    @NamedQuery(name = "Project.findById", query = "SELECT p FROM Project p WHERE p.id = :id"),
    @NamedQuery(name = "Project.findByName", query = "SELECT p FROM Project p WHERE p.name = :name"),
    @NamedQuery(name = "Project.findByUrlSite", query = "SELECT p FROM Project p WHERE p.urlSite = :urlSite"),
    @NamedQuery(name = "Project.findBySummary", query = "SELECT p FROM Project p WHERE p.summary = :summary"),
    @NamedQuery(name = "Project.findByAdditionalComments", query = "SELECT p FROM Project p WHERE p.additionalComments = :additionalComments")})
/**
 * A classe Project descreve informações gerais sobre um projeto que está sendo analisado.
 * @author Cristiano A. Maffort 
 * @version 1.0
 */

public class Project implements Serializable, IGenericEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "urlsite", length = 255)
    private String urlSite;
    
    @Column(name = "summary", columnDefinition="TEXT")
    private String summary;
    
    @Column(name = "additionalcomments", columnDefinition="TEXT")
    private String additionalComments;
    
    @JoinColumn(name = "repository_id", referencedColumnName = "id")
    @ManyToOne
    private Repository repository;
       
    @OneToMany(mappedBy = "project")
    private List<Revision> revisionList;

    public Project() {

    }

    
    public Project(Long id) {
        this.id = id;
    }

    public Project(String name, String urlSite) {
        this.name = name;
        this.urlSite = urlSite;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getPrimaryKey() {
        return this.id;
    }           
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlSite() {
        return urlSite;
    }

    public void setUrlSite(String urlSite) {
        this.urlSite = urlSite;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAdditionalComments() {
        return additionalComments;
    }

    public void setAdditionalComments(String aditionalComments) {
        this.additionalComments = aditionalComments;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @XmlTransient
    public List<Revision> getRevisionList() {
        return revisionList;
    }

    public void setRevisionList(List<Revision> revisionList) {
        this.revisionList = revisionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufmg.dcc.llp.ovisa.dependency.model.domain.Project[ id=" + id + " ]";
    }
    
}
