package br.ufmg.dcc.llp.ovisa.project.model.domain;

import br.ufmg.dcc.llp.util.persistence.jpa.IGenericEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Repository", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"urlAddress"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Repository.findAll", query = "SELECT r FROM Repository r"),
    @NamedQuery(name = "Repository.findById", query = "SELECT r FROM Repository r WHERE r.id = :id"),
    @NamedQuery(name = "Repository.findByUrlAddress", query = "SELECT r FROM Repository r WHERE r.urlAddress = :urlAddress")})
public class Repository implements Serializable, IGenericEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name = "urladdress", length = 255)
    private String urlAddress;
    
    @Column(name = "username")
    private String userName;
    
    @Column(name = "passwd")
    private String password;
    
    @JoinColumn(name = "repositorytype_id", referencedColumnName = "id")
    //@ManyToOne(cascade = CascadeType.PERSIST)
    @ManyToOne
    private RepositoryType repositoryType;
    
    @OneToMany(mappedBy = "repository")
    private List<Project> projectList;

    public Repository() {
    }

    public Repository(Long id) {
        this.id = id;
    }

    public Repository(Long id, String urlAddress) {
        this.id = id;
        this.urlAddress = urlAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getPrimaryKey() {
        return this.id;
    }       
    
    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlTransient
    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Repository)) {
            return false;
        }
        Repository other = (Repository) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufmg.dcc.llp.ovisa.dependency.model.domain.Repository[ id=" + id + " ]";
    }

}
