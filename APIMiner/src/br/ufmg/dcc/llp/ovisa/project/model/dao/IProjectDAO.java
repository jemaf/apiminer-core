package br.ufmg.dcc.llp.ovisa.project.model.dao;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import java.util.List;

public interface IProjectDAO {
    public Project adicionar(Project project);
    public Project atualizar(Project project);
    public Project remover(Project project);
    public Project consultarPorId(Long projectId);
    public Project consultarPorNome(String projectName);
    public List<Project> consultarTodos();
}
