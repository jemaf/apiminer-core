package br.ufmg.dcc.llp.ovisa.project.model.domain;

import br.ufmg.dcc.llp.util.persistence.jpa.IGenericEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "RepositoryType", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"name"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepositoryType.findAll", query = "SELECT r FROM RepositoryType r"),
    @NamedQuery(name = "RepositoryType.findById", query = "SELECT r FROM RepositoryType r WHERE r.id = :id"),
    @NamedQuery(name = "RepositoryType.findByName", query = "SELECT r FROM RepositoryType r WHERE r.name = :name")})
public class RepositoryType implements Serializable, IGenericEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    
    @OneToMany(mappedBy = "repositoryType")
    private List<Repository> repositoryList;

    public RepositoryType() {
    }

    public RepositoryType(Long id) {
        this.id = id;
    }

    public RepositoryType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getPrimaryKey() {
        return this.id;
    }    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Repository> getRepositoryList() {
        return repositoryList;
    }

    public void setRepositoryList(List<Repository> repositoryList) {
        this.repositoryList = repositoryList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepositoryType)) {
            return false;
        }
        RepositoryType other = (RepositoryType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufmg.dcc.llp.ovisa.dependency.model.domain.RepositoryType[ id=" + id + " ]";
    }

}
