package br.ufmg.dcc.llp.ovisa.project.loader;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.IRepositoryTypeDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.ProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.RepositoryTypeDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Repository;
import br.ufmg.dcc.llp.ovisa.project.model.domain.RepositoryType;
import br.ufmg.dcc.llp.util.io.Console;
import br.ufmg.dcc.llp.util.xml.GenericXMLLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class ProjectXMLLoader extends GenericXMLLoader<Project>{
    
    public ProjectXMLLoader(){
    }
    
    public ProjectXMLLoader(File file) throws FileNotFoundException {
        super(file);
    }
    
    public ProjectXMLLoader(String filePath) throws FileNotFoundException {
        super(filePath);
    }
    
    public Project load() {
        return load(Project.class);
    }
    
    public Project load(String filePath) throws FileNotFoundException {        
        return load(Project.class, filePath);
    }
    
    public void store(Project p) {
        store(Project.class, p);
    }
    
    public void store(Project p, String filePath) throws FileNotFoundException {
        store(Project.class, p, filePath);
    }
       
    public static void main(String[] args) {
              
        String menu = "[l|s] [in_file] \r\n" +
                      "l: para carregar projetos a partir de arquivos xml \r\n" + 
                      "   - nesse caso deve-se informar os arquivos de entrada (in_file) \r\n" +
                      "     os quais devem estar separados por ponto e vírgula (;). \r\n" +
                      "s: para gravar projetos em arquivos xml.";
        
        if (args.length == 0)
            System.out.println(menu);
        else if (!args[0].equals("l") && !args[0].equals("s"))
            System.out.println(menu);
        else if (args[0].equals("l")) {
            if (args.length < 2) {
                System.out.println("Obrigatório informar os arquivos de entrada");
                System.out.println(menu);
                return;
            }
            ProjectXMLLoader loader = new ProjectXMLLoader();
            Project p;
            IProjectDAO pdao = new ProjectDAO();
            String[] files = args[1].split(";");
            for (String filePath: files) {
                try {
                    p = loader.load(filePath);
                    pdao.adicionar(p);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            while(true) {
                Project p = new Project();
                System.out.print("Informe o nome do projeto: ");
                p.setName(Console.readString());

                System.out.print("Informe a url do projeto: ");
                p.setUrlSite(Console.readString());

                System.out.print("Informe a url do repositório: "); 
                Repository r = new Repository();
                r.setUrlAddress(Console.readString());
                
                IRepositoryTypeDAO rtdao = new RepositoryTypeDAO();
                List<RepositoryType> rtList = rtdao.consultarTodos();
                int op;
                do {
                    for(int i=0; i<rtList.size(); i++)
                        System.out.println((i+1) + " - " + rtList.get(i).getName());
                    System.out.print("Informe o tipo de repositório: ");
                    op = Console.readInt();
                } while ((op < 1) || (op > rtList.size()));
                r.setRepositoryType(rtdao.consultarPorId(rtList.get(op-1).getId()));
                p.setRepository(r);
                
                ProjectXMLLoader loader = new ProjectXMLLoader();
                try {
                    loader.store(p, p.getName().toLowerCase() + ".xml");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                String exit;
                do {
                    System.out.print("Deseja continuar (S/N): ");
                    exit = Console.readString().toUpperCase();
                    if (exit.equals("N"))
                        return;
                }while (!exit.equals("S"));
            }
        }
    }
}
