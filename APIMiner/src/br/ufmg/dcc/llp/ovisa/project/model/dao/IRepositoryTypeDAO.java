package br.ufmg.dcc.llp.ovisa.project.model.dao;

import br.ufmg.dcc.llp.ovisa.project.model.domain.RepositoryType;
import java.util.List;

public interface IRepositoryTypeDAO {
    public RepositoryType adicionar(RepositoryType repositoryType);
    public RepositoryType atualizar(RepositoryType repositoryType);  
    public RepositoryType remover(RepositoryType repositoryType);
    public RepositoryType consultarPorId(Long repositoryTypeId);
    public RepositoryType consultarPorNome(String repositoryTypeName);
    public List<RepositoryType> consultarTodos();
}
