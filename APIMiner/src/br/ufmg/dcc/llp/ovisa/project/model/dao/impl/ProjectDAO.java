package br.ufmg.dcc.llp.ovisa.project.model.dao.impl;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.IRepositoryDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Repository;
import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;
import java.sql.SQLException;

public class ProjectDAO extends GenericDAO<Project> implements IProjectDAO {
   
    @Override
    protected void prePersist(Project project) {
        IRepositoryDAO rdao = new RepositoryDAO();
        Repository r = rdao.consultarPorURL(project.getRepository().getUrlAddress());
        if(r == null)
            r = rdao.adicionar(project.getRepository());
        project.setRepository(r);        
    }
}
