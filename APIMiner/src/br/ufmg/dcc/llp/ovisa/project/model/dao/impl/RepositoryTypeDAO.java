package br.ufmg.dcc.llp.ovisa.project.model.dao.impl;

import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.IRepositoryTypeDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.RepositoryType;

public class RepositoryTypeDAO extends GenericDAO<RepositoryType> implements IRepositoryTypeDAO {

}
