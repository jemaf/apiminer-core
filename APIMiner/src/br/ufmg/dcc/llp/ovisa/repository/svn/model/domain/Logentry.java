package br.ufmg.dcc.llp.ovisa.repository.svn.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "author",
    "date",
    "paths",
    "msg"
})
@XmlRootElement(name = "logentry")
public class Logentry {

    @XmlElement
    protected String author;
    @XmlElement
    protected String date;
    @XmlElement
    protected Paths paths;
    @XmlElement
    protected String msg;
    @XmlAttribute(required = true)
    protected Long revision;


    public String getAuthor() {
        return author;
    }

     public void setAuthor(String value) {
        this.author = value;
    }

     public String getDate() {
        return date;
    }

    public void setDate(String value) {
        this.date = value;
    }

    public Paths getPaths() {
        return paths;
    }

    public void setPaths(Paths value) {
        this.paths = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String value) {
        this.msg = value;
    }

    public Long getRevision() {
        return revision;
    }

     public void setRevision(Long value) {
        this.revision = value;
    }

}
