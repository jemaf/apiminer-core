package br.ufmg.dcc.llp.ovisa.repository.model.domain;

import br.ufmg.dcc.llp.util.persistence.jpa.IGenericEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Modification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modification.findAll", query = "SELECT m FROM Modification m"),
    @NamedQuery(name = "Modification.findById", query = "SELECT m FROM Modification m WHERE m.id = :id")})
public class Modification implements Serializable, IGenericEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "revision_id", referencedColumnName = "id")
    @ManyToOne
    private Revision revision;

    @Column(name = "actiontype")
    private String actionType;
   
    @JoinColumn(name = "artifact_id", referencedColumnName = "id")
    @ManyToOne
    private Artifact artifact;
    
    public Modification() {
    }

    public Modification(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getPrimaryKey() {
        return this.id;
    }           
    
    public Revision getRevision() {
        return revision;
    }

    public void setRevision(Revision revision) {
        this.revision = revision;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }    
    
    public Artifact getArtifact() {
        return artifact;
    }

    public void setArtifact(Artifact artifact) {
        this.artifact = artifact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Modification)) {
            return false;
        }
        Modification other = (Modification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufmg.dcc.llp.ovisa.dependency.model.domain.Modification[ id=" + id + " ]";
    }

}
