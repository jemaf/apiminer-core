package br.ufmg.dcc.llp.ovisa.repository.model.dao.impl;

import br.ufmg.dcc.llp.ovisa.repository.model.dao.IModificationDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Modification;
import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;

public class ModificationDAO extends GenericDAO<Modification> implements IModificationDAO {

}
