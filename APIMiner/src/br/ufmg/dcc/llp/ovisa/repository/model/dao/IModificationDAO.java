package br.ufmg.dcc.llp.ovisa.repository.model.dao;

import br.ufmg.dcc.llp.ovisa.repository.model.domain.Modification;

public interface IModificationDAO {
    public Modification adicionar(Modification modification);
    public Modification atualizar(Modification modification);
    public Modification consultarPorId(Long modificationId);    
}
