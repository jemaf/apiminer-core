package br.ufmg.dcc.llp.ovisa.repository.model.dao;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Revision;
import java.util.List;

public interface IRevisionDAO {
    public Revision adicionar(Revision revision);
    public Revision atualizar(Revision revision);
    public Revision remover(Revision revision);
    public Revision remover(Project project, Long revisionOrder);
    public Revision consultarPorId(Long revisionId);
    public Revision consultarPorRevisao(Project project, Long revisionOrder);
    public List<Revision> consultarPorProjeto(Project project);
    public Revision consultarUltimaRevisao(Project project);
}
