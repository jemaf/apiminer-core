package br.ufmg.dcc.llp.ovisa.repository.svn.model.domain;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "path"
})
@XmlRootElement(name = "paths")
public class Paths {

    @XmlElement
    protected List<Path> path = new ArrayList<Path>();

    public List<Path> getPath() {
        return this.path;
    }
    
    public void addPath(Path pathItem) {
        path.add(pathItem);
    }

}
