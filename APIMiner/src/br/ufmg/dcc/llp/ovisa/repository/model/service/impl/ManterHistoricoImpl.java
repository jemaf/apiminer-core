package br.ufmg.dcc.llp.ovisa.repository.model.service.impl;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.ProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.IRevisionDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.impl.RevisionDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Revision;
import br.ufmg.dcc.llp.ovisa.repository.model.service.IManterHistorico;
import br.ufmg.dcc.llp.ovisa.repository.svn.loader.SvnLogLoader;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Log;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tmatesoft.svn.core.SVNException;

public class ManterHistoricoImpl implements IManterHistorico {

    private IRevisionDAO rdao = new RevisionDAO();
    
    @Override
    public void atualizar(Long project_id) {
        IProjectDAO pdao = new ProjectDAO();
        Project project = pdao.consultarPorId(project_id);        
        
        // obtem a última revisão armazenada em banco
        Revision revision = rdao.consultarUltimaRevisao(project);        
        
        // se não houver nenhuma revisão no banco, a primeira é 0L
        long lastRevision = 0L;
        // se houver alguma revisão no banco
        if (revision != null)
            lastRevision = revision.getRevisionOrder().longValue();
        
        // carrega log a partir da última revisão
        Log log = null;
        try {
            log = SvnLogLoader.load(project, lastRevision);
        } catch (SVNException ex) {
            Logger.getLogger(ManterHistoricoImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
        
        // exclui a última revisão da lista, já que ela está no banco de dados
        if (lastRevision != 0L)
            for(int i=(log.getLogentry().size()-1); i>=0; i--) {
                if(log.getLogentry().get(i).getRevision().compareTo(revision.getRevisionOrder()) == 0) {
                    log.getLogentry().remove(i);
                    break;
                }
            }
        
        // grava demais registros no banco de dados
        SvnLogLoader.persist(project, log);
    }    
}
