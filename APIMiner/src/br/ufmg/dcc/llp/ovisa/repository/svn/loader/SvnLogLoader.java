package br.ufmg.dcc.llp.ovisa.repository.svn.loader;

import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.ProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.IArtifactDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.IModificationDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.IRevisionDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.impl.ArtifactDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.impl.ModificationDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.impl.RevisionDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Artifact;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Modification;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Revision;
import br.ufmg.dcc.llp.ovisa.repository.svn.SVNUtils;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Log;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Logentry;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Path;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.PersistenceException;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.wc.SVNRevision;

public class SvnLogLoader {
    
    /*
     * Carrega todo o log de versões do repositório
     */
    public static Log load(Project project) throws SVNException {
        return load(project, 0L, SVNRevision.HEAD.getNumber());
    }
    
    /*
     * Carrega a partir de startRevision até a versão atual
     */
    public static Log load(Project project, long startRevision) throws SVNException {
        return load(project, startRevision, SVNRevision.HEAD.getNumber());
    }

    /*
     * Carrega as versões do intervalo informado
     */
    public static Log load(Project project, long startRevision, long endRevision) throws SVNException {
        Log log = new Log();
        
        SVNUtils svn = new SVNUtils(project.getRepository().getUrlAddress());
        Collection logEntries = svn.getHistoryLog(startRevision, endRevision);
        
        for(Iterator entries = logEntries.iterator(); entries.hasNext(); ) {
            SVNLogEntry entry = (SVNLogEntry) entries.next();
            
            Logentry logEntry = new Logentry();

            logEntry.setRevision(entry.getRevision());
            logEntry.setAuthor(entry.getAuthor());
            logEntry.setDate(entry.getDate().toString());
            logEntry.setMsg(entry.getMessage());
                       
            Paths paths = new Paths();
            if (entry.getChangedPaths().size() > 0) {
                Set pathSet = entry.getChangedPaths().keySet();
                
                for(Iterator changedPaths = pathSet.iterator(); changedPaths.hasNext(); ) {
                    SVNLogEntryPath entryPath = (SVNLogEntryPath) entry.getChangedPaths().get(changedPaths.next());
                    Path pathItem = new Path();
                    pathItem.setAction(entryPath.getType() + "");
                    pathItem.setContent(entryPath.getPath());
                    //** falta setar algumas informações...
                    paths.addPath(pathItem);
                }
            }

            logEntry.setPaths(paths);
            log.addLogentry(logEntry);
        }
        
        return log;        
    }
    
    /*
     * Grava log no banco de dados
     */
    public static void persist(Project project, Log log) {
            
        IRevisionDAO rdao = new RevisionDAO();
        IArtifactDAO adao = new ArtifactDAO();
        IModificationDAO mdao = new ModificationDAO();        
        
        for(Logentry logentry: log.getLogentry()) {

            Revision r = new Revision();
            r.setProject(project);
            if (logentry.getAuthor() == null)
                r.setAuthor("");
            else
                r.setAuthor(logentry.getAuthor());
            
            r.setDateCommit(logentry.getDate());
            r.setMessage(logentry.getMsg());
            r.setRevisionOrder(logentry.getRevision());
            try {
                r = rdao.adicionar(r);
            } catch(PersistenceException pe) {
                rdao.remover(project, r.getRevisionOrder());
                r = rdao.adicionar(r);
            }
            
            if (logentry.getPaths() != null) {
                List<Path> phs = logentry.getPaths().getPath();

                for(Path ph: phs) {
                    Modification m = new Modification();
                    m.setRevision(r);
                    m.setActionType(ph.getAction());

                    Artifact a = adao.consultarPorFilePath(ph.getContent());

                    if (a == null) {
                        a = new Artifact();
                        a.setFilePath(ph.getContent());
                        a = adao.adicionar(a);
                    }

                    m.setArtifact(a);

                    mdao.adicionar(m);
                }
            }
        }        
    }
    
    public static void process(String projectName) {

        IProjectDAO pdao = new ProjectDAO();
        Project project = pdao.consultarPorNome(projectName);
        
        if (project == null)
            throw new RuntimeException("Projeto '"+ projectName +"' não encontrado no banco de dados.");
      
        Log log = null;
        try {
            log = SvnLogLoader.load(project); // carrega dados diretamente do repositório
        } catch(SVNException e) {
            throw new RuntimeException(e);
        }

        SvnLogLoader.persist(project, log);
    }
}
