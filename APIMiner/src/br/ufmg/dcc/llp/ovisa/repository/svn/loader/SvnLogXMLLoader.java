package br.ufmg.dcc.llp.ovisa.repository.svn.loader;

import br.ufmg.dcc.llp.ovisa.project.loader.ProjectXMLLoader;
import br.ufmg.dcc.llp.ovisa.project.model.dao.IProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.dao.impl.ProjectDAO;
import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.repository.svn.model.domain.Log;
import br.ufmg.dcc.llp.util.xml.GenericXMLLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SvnLogXMLLoader extends GenericXMLLoader<Log>{
    
    public SvnLogXMLLoader(File file) throws FileNotFoundException {
        super(file);
    }
    
    public SvnLogXMLLoader(String filePath) throws FileNotFoundException {
        super(filePath);
    }
    
    public SvnLogXMLLoader() {  }
    
    public Log load() {
        return load(Log.class);
    }
      
    public void store(Log p) {
        store(Log.class, p);
    }
    
    public void process(String projectName) {
            
        IProjectDAO pdao = new ProjectDAO();

        Project project = pdao.consultarPorNome(projectName);
        
        if (project == null)
            throw new RuntimeException("Projeto '" + projectName + "' não encontrado no banco de dados.");
        
        Log log = this.load();
        
        SvnLogLoader.persist(project, log);

    }
        
    public static void main(String[] args) {
        try {
            ProjectXMLLoader ploader = new ProjectXMLLoader();
            Project p = ploader.load(args[0]);            

            SvnLogXMLLoader sl = new SvnLogXMLLoader(args[1]);
            sl.process(p.getName());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SvnLogXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SvnLogXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
