package br.ufmg.dcc.llp.ovisa.repository.svn;

import java.io.File;
import java.util.Collection;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class SVNUtils {
    
    private String svnURL;          // url do repositório
    private File destPath;          // diretório de destino
    private String userName;        // nome do usuário
    private String password;        // senha de acesso ao repositório
    
    private static SVNClientManager clientManager = SVNClientManager.newInstance();    
    SVNRepository repository = null;
    
    public SVNUtils(String svnURL) throws SVNException {
       this.svnURL = svnURL;
       this.loadRepository();
    }

    public File getDestinationPath() {
        return destPath;
    }

    public void setDestinationPath(File dstPath) {
        this.destPath = dstPath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSVNURL() {
        return svnURL;
    }

    public void setSVNURL(String svnURL) {
        this.svnURL = svnURL;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public long checkout(long checkoutToRevision) throws SVNException {
        SVNUpdateClient updateClient = clientManager.getUpdateClient( );
        SVNRevision r = SVNRevision.create(checkoutToRevision);
        return updateClient.doCheckout(SVNURL.parseURIEncoded(svnURL), destPath, r, r, SVNDepth.UNKNOWN, true);
    }
    
    public long update(long updateToRevision) throws SVNException {
        SVNUpdateClient updateClient = clientManager.getUpdateClient();
        SVNRevision r = SVNRevision.create(updateToRevision);
        return updateClient.doUpdate(destPath, r, SVNDepth.UNKNOWN, true, true);
    }
    
    public Collection getHistoryLog(long startRevision, long endRevision) throws SVNException {
        Collection entries = this.repository.log(new String[] {""}, null, startRevision, endRevision, true, true);
        return entries;
    }
    
    private void loadRepository() throws SVNException {
        String protocol = this.svnURL.substring(this.svnURL.indexOf(":"));
              
        if (protocol.contains("svn")) {
            SVNRepositoryFactoryImpl.setup();
            repository = SVNRepositoryFactoryImpl.create(SVNURL.parseURIDecoded(this.svnURL));
            
        }
        else if (protocol.startsWith("http")) {
            DAVRepositoryFactory.setup();
            repository = DAVRepositoryFactory.create(SVNURL.parseURIEncoded(this.svnURL));
        }
        else if (protocol.equals("file")) {
            FSRepositoryFactory.setup();
            repository = FSRepositoryFactory.create(SVNURL.parseURIEncoded(this.svnURL));
        }
        else
            throw new RuntimeException("Protocolo da url não eh válido: "+ protocol);

        if ((userName!=null) && !userName.isEmpty()) {
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(this.userName, this.password);
            repository.setAuthenticationManager(authManager);
        }        
    }
    
    
    public static void main(String[] args) {
        
        /*  // get log
        SVNUtils svn = new SVNUtils("https://pmd.svn.sourceforge.net/svnroot/pmd/trunk/pmd");
        
        try {
            Collection logEntries = svn.getHistoryLog(7629L, 7648L);
            
            for(Iterator entries = logEntries.iterator(); entries.hasNext(); ) {
                SVNLogEntry logEntry = (SVNLogEntry) entries.next();
                System.out.println(logEntry.getRevision());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
         */
        
       
        try {
            // checkout
            SVNUtils svn = new SVNUtils("https://pmd.svn.sourceforge.net/svnroot/pmd/trunk/pmd");
            svn.setDestinationPath(new File("/home/cristiano/pmd"));
            
            //svn.checkout(4L);
            
            System.out.println(svn.update(SVNRevision.HEAD.getNumber()));
            
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        
/*        
        String destPath = "/home/cristiano/temp/pmd";
        String svnURL = "https://pmd.svn.sourceforge.net/svnroot/pmd/trunk/pmd";
               
        try {
            SVNRepositoryFactoryImpl.setup(); // p/ svn://
            //DAVRepositoryFactory.setup();     // p/ http://, https://
            //FSRepositoryFactory               // p/ file:///
            
            SVNRepository repository = DAVRepositoryFactory.create(SVNURL.parseURIEncoded(svnURL));
            
            Collection logEntries = repository.log(new String[] {""}, null, 0L, -1L, true, true);
            
            for(Iterator entries = logEntries.iterator(); entries.hasNext(); ) {
                SVNLogEntry logEntry = (SVNLogEntry) entries.next();
                System.out.println(logEntry.getRevision());
            }
                    
            //long revision = SVNUtils.checkout(SVNURL.parseURIEncoded(svnURL), SVNRevision.HEAD, new File(destPath), true);
            //System.out.println("checkout revision " + revision + " concluded.");
        } catch(Exception e) {
            e.printStackTrace();
        }
 * 
 */
    }

}


