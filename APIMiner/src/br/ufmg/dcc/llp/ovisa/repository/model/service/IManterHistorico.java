package br.ufmg.dcc.llp.ovisa.repository.model.service;

public interface IManterHistorico {
    public void atualizar(Long project_id);
}
