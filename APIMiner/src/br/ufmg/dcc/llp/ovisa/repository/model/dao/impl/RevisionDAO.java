package br.ufmg.dcc.llp.ovisa.repository.model.dao.impl;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.ovisa.repository.model.dao.IRevisionDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Revision;
import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;
import java.util.List;
import javax.persistence.Query;

public class RevisionDAO extends GenericDAO<Revision> implements IRevisionDAO {

    @Override
    public Revision remover(Project project, Long revisionOrder) {
               
        Query qry = em.createNamedQuery("Revision.findByRevisionOrderAndProject", Revision.class);
        qry.setParameter("revisionOrder", revisionOrder);
        qry.setParameter("project", project);

        if (em.getTransaction().isActive())
            em.getTransaction().rollback();
        
        if (qry.getResultList().isEmpty())
            return null;
                       
        Revision revision = (Revision) qry.getSingleResult();
        
        return remover(revision);           
    }

    @Override
    public Revision consultarPorRevisao(Project project, Long revisionOrder) {
        Query qry = this.em.createNamedQuery("Revision.findByRevisionOrderAndProject", Revision.class);
        qry.setParameter("revisionOrder", revisionOrder);
        qry.setParameter("project", project);
        
        if (qry.getResultList().isEmpty())
            return null;
        
        return (Revision)qry.getSingleResult();
    }

    @Override
    public List<Revision> consultarPorProjeto(Project project) {
        Query qry = this.em.createNamedQuery("Revision.findByfindByProject", Revision.class);
        qry.setParameter("project", project);
        
        return qry.getResultList();        
    }

    @Override
    public Revision consultarUltimaRevisao(Project project) {

        String qryStr = "SELECT r.* " +
                        "  FROM revision r, (SELECT project_id, MAX(revisionorder) as lastrevorder " +
			"                      FROM revision " +
                        "                     WHERE project_id = " + project.getId() +
			"                    GROUP BY project_id) as lastrevision " +
                        " WHERE r.project_id = lastrevision.project_id " +
                        "   AND r.revisionorder = lastrevision.lastrevorder;";
        
        Query qry = this.em.createNativeQuery(qryStr, Revision.class);
                
        if(qry.getResultList().isEmpty())
            return null;
        
        return (Revision)qry.getSingleResult();       
    }
}
