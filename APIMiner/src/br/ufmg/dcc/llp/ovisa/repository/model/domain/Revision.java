package br.ufmg.dcc.llp.ovisa.repository.model.domain;

import br.ufmg.dcc.llp.ovisa.project.model.domain.Project;
import br.ufmg.dcc.llp.util.persistence.jpa.IGenericEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "Revision")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revision.findAll", query = "SELECT r FROM Revision r"),
    @NamedQuery(name = "Revision.findById", query = "SELECT r FROM Revision r WHERE r.id = :id"),
    @NamedQuery(name = "Revision.findByRevisionOrder", query = "SELECT r FROM Revision r WHERE r.revisionOrder = :revisionOrder"),
    @NamedQuery(name = "Revision.findByRevisionOrderAndProject", query = "SELECT r FROM Revision r WHERE r.revisionOrder = :revisionOrder AND r.project = :project"),    
    @NamedQuery(name = "Revision.findByProject", query = "SELECT r FROM Revision r WHERE r.project = :project"),    
    @NamedQuery(name = "Revision.findByAuthor", query = "SELECT r FROM Revision r WHERE r.author = :author"),
    @NamedQuery(name = "Revision.findByDateCommit", query = "SELECT r FROM Revision r WHERE r.dateCommit = :dateCommit"),
    @NamedQuery(name = "Revision.findByMessage", query = "SELECT r FROM Revision r WHERE r.message = :message")})
public class Revision implements Serializable, IGenericEntity {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    
    @Column(name = "revisionorder")
    private Long revisionOrder;   
   
    @Column(name = "author", length = 255)
    private String author;

    @Column(name = "datecommit", length = 50)
    private String dateCommit;

    @Column(name = "message", columnDefinition="TEXT")
    private String message;

    @OneToMany(mappedBy = "revision")
    private List<Modification> modificationList;
      
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @ManyToOne
    private Project project;

    public Revision() {
    }

    public Revision(Long id) {
        this.id = id;
    }

    public Revision(Long revisionOrder, String author, String dateCommit) {
        this.revisionOrder = revisionOrder;
        this.author = author;
        this.dateCommit = dateCommit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getPrimaryKey() {
        return this.id;
    }           
    
    public Long getRevisionOrder() {
        return revisionOrder;
    }

    public void setRevisionOrder(Long revisionOrder) {
        this.revisionOrder = revisionOrder;
    }    
    
    
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDateCommit() {
        return dateCommit;
    }

    public void setDateCommit(String dateCommit) {
        this.dateCommit = dateCommit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @XmlTransient
    public List<Modification> getModificationList() {
        return modificationList;
    }

    public void setModificationList(List<Modification> modificationList) {
        this.modificationList = modificationList;
    }
     
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revision)) {
            return false;
        }
        Revision other = (Revision) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.ufmg.dcc.llp.ovisa.dependency.model.domain.Revision[ id=" + id + " ]";
    }
    
}
