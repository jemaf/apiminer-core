package br.ufmg.dcc.llp.ovisa.repository.svn.model.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "path")
public class Path {

    @XmlValue
    protected String content;
    
    @XmlAttribute
    protected String action;
    @XmlAttribute
    protected String copyfromPath;
    @XmlAttribute
    protected Integer copyfromRev;
    @XmlAttribute
    protected String kind;

    public String getContent() {
        return content;
    }

    public void setContent(String value) {
        this.content = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String value) {
        this.action = value;
    }

    public String getCopyfromPath() {
        return copyfromPath;
    }

    public void setCopyfromPath(String value) {
        this.copyfromPath = value;
    }

    public Integer getCopyfromRev() {
        return copyfromRev;
    }

    public void setCopyfromRev(Integer value) {
        this.copyfromRev = value;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String value) {
        this.kind = value;
    }

}
