package br.ufmg.dcc.llp.ovisa.repository.model.dao.impl;

import br.ufmg.dcc.llp.ovisa.repository.model.dao.IArtifactDAO;
import br.ufmg.dcc.llp.ovisa.repository.model.domain.Artifact;
import br.ufmg.dcc.llp.util.persistence.jpa.GenericDAO;
import javax.persistence.Query;

public class ArtifactDAO extends GenericDAO<Artifact> implements IArtifactDAO {

    @Override
    public Artifact consultarPorFilePath(String artifactFilePath) {
        Query qry = em.createNamedQuery("Artifact.findByFilePath", Artifact.class);
        qry.setParameter("filePath", artifactFilePath);
        
        if (qry.getResultList().isEmpty())
            return null;
               
        return (Artifact)qry.getSingleResult();
    }
}
