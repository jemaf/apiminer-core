package br.ufmg.dcc.llp.ovisa.repository.svn.model.domain;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "logentry"
})
@XmlRootElement(name = "log")
public class Log {

    @XmlElement(required = true)
    protected List<Logentry> logentry = new ArrayList<Logentry>();

    public List<Logentry> getLogentry() {
        return this.logentry;
    }
    
    public void addLogentry(Logentry logentryItem) {
        logentry.add(logentryItem);
    }

}
