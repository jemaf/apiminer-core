package br.ufmg.dcc.llp.ovisa.repository.model.dao;

import br.ufmg.dcc.llp.ovisa.repository.model.domain.Artifact;

public interface IArtifactDAO {
    public Artifact adicionar(Artifact artifact);
    public Artifact atualizar(Artifact artifact);
    public Artifact remover(Artifact artifact);
    public Artifact consultarPorId(Long artifactId);
    public Artifact consultarPorFilePath(String artifactName); 
}
