/**
 * 
 */

package br.ufmg.dcc.llp.apiminer.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.ufmg.dcc.llp.apiminer.build.IBuild;
import br.ufmg.dcc.llp.apiminer.build.android.AndroidBuild;

/**
 * @author edumontandon
 * 
 */
public class Util {

	private static final Logger utilLog = LoggerFactory.getLogger(Util.class);

	private static Document parseXmlFile() {

		try {
			// get the factory
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			
			// Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream is = classLoader.getResourceAsStream("META-INF/config.xml");
			
			// parse using builder to get DOM representation of the XML file
			return db.parse(is);
		} catch (ParserConfigurationException pce) {
			utilLog.error("Problem with loading of config.xml file:", pce);
		} catch (SAXException se) {
			utilLog.error("Problem with loading of config.xml file:", se);
		} catch (IOException ioe) {
			utilLog.error("Problem with loading of config.xml file:", ioe);
		}

		return null;
	}
	
	/**
	 * @param repName
	 * @return
	 */
	public static String getRepositoryExecutablePath(String repName) {
		
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl = ((Element) elem.getElementsByTagName("repository-types").item(0))
		        .getElementsByTagName("property");
		
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {

				Element prop = (Element) nl.item(i);
				if (prop.getAttribute("name").equalsIgnoreCase(repName))
					return prop.getAttribute("value");
			}
		}
		
		return null;
	}
	
	public static IBuild getBuildInterface() {
		
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl = ((Element) elem.getElementsByTagName("repository").item(0))
		        .getElementsByTagName("property");

		String value = null;
		
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength() && value == null; i++) {

				Element prop = (Element) nl.item(i);
				if (prop.getAttribute("name").equals("build-interface"))
					value = prop.getAttribute("value");
			}
		}
		
		if(value.equals("android"))
			return new AndroidBuild();
		else
			return null;
	}
	

	/**
	 * @return
	 */
	public static int getMaxLoopValue() {
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl = ((Element) elem.getElementsByTagName("annotation").item(0))
		        .getElementsByTagName("property");

		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {

				Element prop = (Element) nl.item(i);
				if (prop.getAttribute("name").equals("maxloop"))
					return Integer.parseInt(prop.getAttribute("value"));
			}
		}
		return 0;
	}

	/**
	 * @return
	 */
	public static String getRepositoryPath() {
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl = ((Element) elem.getElementsByTagName("repository").item(0))
		        .getElementsByTagName("property");

		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {

				Element prop = (Element) nl.item(i);
				if (prop.getAttribute("name").equals("systems-path"))
					return prop.getAttribute("value");
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	public static String getProjectsClasspath() {
		String jars = "";
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl2 = ((Element) ((Element) elem.getElementsByTagName("repository").item(0))
		        .getElementsByTagName("classpath").item(0)).getElementsByTagName("property");

		if (nl2 != null && nl2.getLength() > 0)
			for (int i = 0; i < nl2.getLength(); i++) {

				Element jar = (Element) nl2.item(i);
				if (jar.getAttribute("name").equals("jar"))
					jars += jar.getAttribute("value") + " ";
			}
		return jars.trim();
	}
	
	/**
	 * @return
	 */
	public static String getProjectsSourcepath() {
		String jars = "";
		Element elem = parseXmlFile().getDocumentElement();
		NodeList nl2 = ((Element) ((Element) elem.getElementsByTagName("repository").item(0))
		        .getElementsByTagName("sourcepath").item(0)).getElementsByTagName("property");

		if (nl2 != null && nl2.getLength() > 0)
			for (int i = 0; i < nl2.getLength(); i++) {

				Element jar = (Element) nl2.item(i);
				if (jar.getAttribute("name").equals("sp"))
					jars += jar.getAttribute("value") + " ";
			}
		return jars.trim();
	}

	/**
	 * Convert a string to html format (ignore special symbols, like > and <
	 * 
	 * @param param
	 * @return
	 */
	public static String strToHtml(String param) {
		return param.replace("<", "&lt;").replace(">", "&gt;");
	}

	/**
	 * Convert the html text to normal text
	 * 
	 * @param param
	 * @return
	 */
	public static String htmlToStr(String param) {
		return param.replace("&lt;", "<").replace("&gt;", ">");
	}

	/**
	 * Treat the parameters that are used as arguments of a program
	 * 
	 * @param args
	 *            argument list
	 * @return map of parameter/arguments
	 */
	public static Map<String, ArrayList<String>> handleEntries(String args[]) {

		Map<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();

		String param = null;
		for (String argument : args) {

			if (argument.startsWith("-"))
				param = argument;
			else {
				ArrayList<String> temp = result.get(param) == null ? new ArrayList<String>()
				        : result.get(param);

				temp.add(argument);
				result.put(param, temp);
			}
		}

		return result;
	}

	/**
	 * Search for the files that are child of filName based on a extension
	 * passed as parameter
	 * 
	 * @param fileName
	 *            the root's file name of the search
	 * @param postFix
	 *            the extension of the file
	 * @return a list of strings with the full path of the files
	 */
	public static ArrayList<String> collectFiles(String fileName, String postFix) {
		ArrayList<String> filesName = new ArrayList<String>();
		File f = new File(fileName);

		if (f.isFile() && f.getName().endsWith(postFix)) {
			filesName.add(f.getAbsolutePath());
		} else if (f.isDirectory()) {
			for (File child : f.listFiles()) {
				filesName.addAll(collectFiles(child.getAbsolutePath(), postFix));
			}
		}

		return filesName;
	}

	/**
	 * Execute a single command at terminal
	 * 
	 * @param string
	 *            the command that will be executed
	 * @return true if it executed normally, false otherwise
	 */
	public static boolean executeInTerminal(String command) {
		utilLog.debug("[TERMINAL: " + command + "]");

		Process ls = null;

		try {

			ls = Runtime.getRuntime().exec(command);

			StreamGobbler out = new StreamGobbler(ls.getInputStream(), "OUTPUT");
			StreamGobbler err = new StreamGobbler(ls.getErrorStream(), "ERROR");
			out.run();
			err.run();
			ls.waitFor();

		} catch (IOException e1) {
			utilLog.error("IO Error when try to execute terminal command", e1);
		} catch (InterruptedException e) {
			utilLog.error("IO Error when try to execute terminal command", e);
			e.printStackTrace();
		}

		if (ls != null && ls.exitValue() == 0)
			return true;
		else
			return false;
	}			
}


class StreamGobbler extends Thread {
	InputStream is;
	String type;
	Logger log = LoggerFactory.getLogger(StreamGobbler.class);

	StreamGobbler(InputStream is, String type) {
		this.is = is;
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
				log.debug(type + ">" + line);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
