package br.ufmg.dcc.llp.apiminer.ranking;

import br.ufmg.dcc.llp.apiminer.jpa.Example;

public class LocRanking implements IRanking {

	/**
	 * loc relevance factor.
	 */
	public static final int LOC_FACTOR = 4;
	
	/**
	 * example that will be ranked.
	 */
	private Example example;

	/**
	 * Class' constructor.
	 * 
	 * @param ex1
	 *            the example which the rank will be calculated
	 */
	public LocRanking(Example ex1) {
		this.example = ex1;
	}

	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#calculateRanking()
	 */
	@Override
	public double calculateRanking() {
		int result = 0;

		int loc = this.example.getExample().split("\n").length;

		if (loc >= 4 && loc <= 8) {
			result = 10;
		} else if (loc > 30) {
			result = 0;
		} else if (loc == 1) {
			result = 4;
		} else if (loc >= 2 && loc <= 3) {
			result = 5;
		} else if (loc >= 9 && loc <= 10) {
			result = 7;
		}

		return result * LOC_FACTOR;
	}
	
	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#getFactor()
	 */
	@Override
	public int getFactor() {
		return LOC_FACTOR;
	}

}
