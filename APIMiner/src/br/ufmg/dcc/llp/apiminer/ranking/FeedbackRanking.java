package br.ufmg.dcc.llp.apiminer.ranking;

import java.util.ArrayList;
import java.util.List;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.ExampleFeedback;

public class FeedbackRanking implements IRanking {

	/**
	 * feedback factor.
	 */
	private static final int FEEDBACK_FACTOR = 2;

	/**
	 * threshold to consider the feedback rating.
	 */
	private static final int FEEDBACK_MINIMUM = 5;

	/**
	 * example that will be ranked.
	 */
	private Example example;

	/**
	 * Class constructor
	 * 
	 * @param ex
	 *            the example that will be ranked
	 */
	public FeedbackRanking(Example ex) {
		this.example = ex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#calculateRanking()
	 */
	@Override
	public double calculateRanking() {

		int numFeedbacks = 0;

		List<ExampleFeedback> feedbacks = this.example.getExampleFeedbacks() == null ? 
				new ArrayList<ExampleFeedback>() : this.example.getExampleFeedbacks();

		for (ExampleFeedback feedback : feedbacks) {
			numFeedbacks += feedback.getRating();
		}

		int feedbackSize = feedbacks.size() <= FEEDBACK_MINIMUM ? 1 : feedbacks
				.size();

		return (numFeedbacks * 2 / feedbackSize * FEEDBACK_FACTOR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#getFactor()
	 */
	@Override
	public int getFactor() {
		return FEEDBACK_FACTOR;
	}

}
