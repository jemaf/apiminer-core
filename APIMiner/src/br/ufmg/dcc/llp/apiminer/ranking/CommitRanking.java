package br.ufmg.dcc.llp.apiminer.ranking;

import br.ufmg.dcc.llp.apiminer.jpa.Example;

/**
 * @author edumontandon
 *
 */
public class CommitRanking implements IRanking {

	/**
	 * example that will be ranked.
	 */
	private Example example;
	
	/**
	 * commit factor.
	 */
	public static final int COMMIT_FACTOR = 2;

	/**
	 * Class' constructor
	 * @param ex example that will be ranked.
	 */
	public CommitRanking(Example ex) {
		this.example = ex;
	}

	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#calculateRanking()
	 */
	@Override
	public double calculateRanking() {
		return this.example.getNumFileCheckout() * COMMIT_FACTOR;
	}
	
	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#getFactor()
	 */
	@Override
	public int getFactor() {
		return COMMIT_FACTOR;
	}

}
