package br.ufmg.dcc.llp.apiminer.ranking;

/**
 * Ranking's interface. Every ranking instance should implement this interface.
 * 
 * @author edumontandon
 * 
 */
public interface IRanking {

	/**
	 * Calculate the example's ranking.
	 * 
	 * @return the value of the example
	 */
	public double calculateRanking();

	/**
	 * Return the ranking's factor
	 * 
	 * @return the ranking's factor
	 */
	public int getFactor();

}
