package br.ufmg.dcc.llp.apiminer.ranking;

import br.ufmg.dcc.llp.apiminer.jpa.Example;

/**
 * Class that ranks based on number of downloads of the project.
 * 
 * @author edumontandon
 * 
 */
public class DownloadRanking implements IRanking {

	/**
	 * download factor.
	 */
	public static final int DOWNLOAD_FACTOR = 2;

	/**
	 * example that will be ranked.
	 */
	private Example example;

	/**
	 * Class constructor
	 * 
	 * @param ex
	 *            the example that will be ranked
	 */
	public DownloadRanking(Example ex) {
		this.example = ex;
	}

	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#calculateRanking()
	 */
	@Override
	public double calculateRanking() {

		int rank = 0;

		Long value = this.example.getRepository().getProjects().get(0)
				.getNumDownload();
		long numDl = value == null ? 0 : value;

		if (numDl >= 5000000)
			rank = 10;
		else if (numDl >= 2000000)
			rank = 8;
		else if (numDl >= 1000000)
			rank = 7;
		else if (numDl >= 500000)
			rank = 6;
		else if (numDl >= 250000)
			rank = 5;
		else if (numDl >= 50000)
			rank = 4;
		else if (numDl >= 25000)
			rank = 3;
		else if (numDl >= 7500)
			rank = 2;
		else
			rank = 0;

		return rank * DOWNLOAD_FACTOR;
	}

	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#getFactor()
	 */
	@Override
	public int getFactor() {
		return DOWNLOAD_FACTOR;
	}

}
