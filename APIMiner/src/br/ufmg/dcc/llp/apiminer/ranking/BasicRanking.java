package br.ufmg.dcc.llp.apiminer.ranking;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic Ranking class.
 * 
 * @author edumontandon
 * 
 */
public final class BasicRanking implements IRanking {

	/**
	 * object that will rank the example.
	 */
	private List<IRanking> rankings;

	/**
	 * Class constructor which receives the example that will be ranked.
	 */
	public BasicRanking() {
		rankings = new ArrayList<IRanking>();
	}

	/**
	 * Add a ranking into the list.
	 * 
	 * @param ranking
	 *            the ranking that will be added
	 */
	public final void addRanking(IRanking ranking) {
		this.rankings.add(ranking);
	}

	/**
	 * remove a ranking from the list.
	 * 
	 * @param ranking
	 *            ranking that will be removed
	 */
	public final void removeRanking(IRanking ranking) {
		this.rankings.remove(ranking);
	}

	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#calculateRanking()
	 */
	public double calculateRanking() {
		double result = 0;
		
		for (IRanking ranking : this.rankings) {
			result += ranking.calculateRanking();
		}
		
		return result;
	}



	/* (non-Javadoc)
	 * @see br.ufmg.dcc.llp.apiminer.controller.ranking.IRanking#getFactor()
	 */
	public int getFactor() {
		int factor = 0;
		
		for (IRanking ranking : this.rankings) {
			factor += ranking.getFactor();
		}
		
		return factor;
	}

}