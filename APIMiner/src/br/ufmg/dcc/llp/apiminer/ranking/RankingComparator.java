package br.ufmg.dcc.llp.apiminer.ranking;

import java.util.Comparator;

import br.ufmg.dcc.llp.apiminer.jpa.Example;

/**
 * Class that compares two Examples.
 * 
 * @author edumontandon
 * 
 */
public class RankingComparator implements Comparator<Example> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Example ex1, Example ex2) {

		// calculate the ranking of first example
		BasicRanking b1 = new BasicRanking();
		b1.addRanking(new LocRanking(ex1));
		b1.addRanking(new CommitRanking(ex1));
		b1.addRanking(new FeedbackRanking(ex1));
		b1.addRanking(new DownloadRanking(ex1));

		// calculate the ranking of the second example
		BasicRanking b2 = new BasicRanking();
		b2.addRanking(new LocRanking(ex2));
		b2.addRanking(new CommitRanking(ex2));
		b2.addRanking(new FeedbackRanking(ex2));
		b2.addRanking(new DownloadRanking(ex2));

		double v1 = b1.calculateRanking() / b1.getFactor();
		double v2 = b2.calculateRanking() / b2.getFactor();

		return v1 > v2 ? -1 : v1 == v2 ? 0 : 1;
	}
}
