/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.dao.ApiClassDAO;
import br.ufmg.dcc.llp.apiminer.dao.ApiMethodDAO;
import br.ufmg.dcc.llp.apiminer.dao.ExampleDAO;
import br.ufmg.dcc.llp.apiminer.jpa.ApiClass;
import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.ranking.RankingComparator;

/**
 * @author edumontandon
 *
 */
public class ExamplesQuery {
	
	private static final Logger log = LoggerFactory.getLogger(ExamplesQuery.class);
	
	/**
	 * Execute the process to consult examples from determined method
	 * 
	 * @param methodSignature
	 * @return
	 */
	public static List<Example> queryExample(String methodSignature) {
		log.info("Querying examples of use of " + methodSignature);
		
		List<Example> result = new ArrayList<Example>();

		String typeAndName = methodSignature.split("\\(")[0];
		String method = typeAndName.substring(typeAndName.lastIndexOf(".") + 1,
		        typeAndName.length());
		String classe = typeAndName.substring(0, typeAndName.lastIndexOf("."));

		method += methodSignature.subSequence(methodSignature.indexOf('('),
		        methodSignature.length());

		ApiClassDAO classDAO = new ApiClassDAO();
		ApiMethodDAO methodDAO = new ApiMethodDAO();
		ExampleDAO exampleDAO = new ExampleDAO();

		ApiClass clazz = new ApiClass();
		clazz.setName(classe);

		List<ApiClass> apiClasses = classDAO.select(clazz);

		if (apiClasses.size() > 0) {
			ApiMethod apiMethod = new ApiMethod();
			apiMethod.setApiClass(apiClasses.get(0));
			apiMethod.setFqn(method);
			List<ApiMethod> apiMethods = methodDAO.select(apiMethod);

			if (apiMethods.size() > 0) {
				Example tempExample = new Example();
				tempExample.setApiMethod(apiMethods.get(0));
				result = exampleDAO.select(tempExample);
			}
		}
		
		//sort the examples according to the comparator
		Collections.sort(result, new RankingComparator());
		
		log.info(result.size() + "Examples found");
		return result;
	}


}
