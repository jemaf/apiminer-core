/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.main;

import java.util.ArrayList;
import java.util.Collection;

import br.ufmg.dcc.llp.apiminer.annotation.parser.JDTParser;
import br.ufmg.dcc.llp.apiminer.jpa.Example;

/**
 * @author edumontandon
 * 
 */
public class RealTimeProcessing {

	private static String[] SYSTEMS = new String[] {
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/wordpress",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/alienbloodbath",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/nethack-android",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/apg",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/talkmyphone",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/OtRChat",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/iosched",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Twitli",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/zxing",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/VoyagerConnect",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Yaaic",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/swallowcatcher",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Audalyzer",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/replica-island",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/sms-backup-plus",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/shortyz",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Yams",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/sokoban",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/exchangeit",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/tomdroid",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Orbot",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Chime-Timer",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/dialer2",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/transdroid",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/gddsched",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/open-ar",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/twisty",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/and-bible",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Tumblife",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Vector-Pinball",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/sgtpuzzles",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/nethack",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/robotfindskitten",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/mustard",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/scrambled-net",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/lexic",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/newtons-cradle",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/frozenbubble",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/androidsfortune",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/connectbot",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/swiftp",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/beem",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/wordsearch-android",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/TouchTest",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/asqare",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/tiltmazes",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/linphone-android",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/corporateaddressbook",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/BansheeRemote",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/gcall-call-logger",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/solitaire-for-android",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Vidiom",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/wordseek",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/k-9",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/sipdroid",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/opensudoku-android",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/android-target",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Dazzle",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/andtweet",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/hotdeath",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Crowdroid2.0",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Substrate",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/spelldial",
	        "/Users/edumontandon/Mestrado/repositorio-android/sistemas/Android-Terminal-Emulator" };

	private static String CP = "/Developer/Android/android-sdk/platforms/android-8/android.jar";

	/**
	 * A vector of one position with the method signature at it.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length == 1) {
			Collection<Example> result = execute(args[0]);
			for (Example example : result) {
				System.out.println(example);
				System.out.println("=================================================");
			}
		}

	}

	/**
	 * @param methodSignature
	 * @return
	 */
	public static Collection<Example> execute(String methodSignature) {

		methodSignature = methodSignature.trim();
		
		JDTParser parser;
		Collection<Example> result = new ArrayList<Example>();

		for (String system : SYSTEMS) {
			String param = "-files " + system + " -m " + methodSignature + " -cp " + CP + " -sp "
			        + system;

			parser = new JDTParser(param.split(" "));
			parser.parse();
			result.addAll(parser.getExamples());
		}

		return result;
	}

	
	public static String[] getSystems() {
		return SYSTEMS;
	}
	
}
