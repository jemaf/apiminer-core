package br.ufmg.dcc.llp.apiminer.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.compress.utils.Charsets;

import br.ufmg.dcc.llp.apiminer.build.android.AndroidBuild;
import br.ufmg.dcc.llp.apiminer.dao.ApiClassDAO;
import br.ufmg.dcc.llp.apiminer.dao.ApiMethodDAO;
import br.ufmg.dcc.llp.apiminer.dao.RepositoryTypeDAO;
import br.ufmg.dcc.llp.apiminer.jpa.ApiClass;
import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.jpa.Repository;
import br.ufmg.dcc.llp.apiminer.jpa.Repositorytype;

/**
 * Class that provide a console view of the application to the user.
 * 
 * @author edumontandon
 * 
 */
public class Console {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("APIMiner Console");
		System.out.println("1 - Add Project");
		System.out.println("2 - Add API Method");
		System.out.println("3 - Add Many Projects (Based on CSV File)");
		System.out.println("4 - Add Many Methods (Based on CSV File)");
		System.out.println("Other - Exit");

		int opt = 0;

		Scanner scanner = new Scanner(System.in);
		opt = scanner.nextInt();

		switch (opt) {

		case 1:
			addProjectByConsole();
			break;

		case 2:
			addApiMethodByConsole();
			break;

		case 3:
			addProjects();
			break;

		case 4:
			addMethods();
			break;

		default:
			break;
		}
		scanner.close();

		System.out.println("Goodbye!!");
	}

	/**
	 * Add many projects based on csv file
	 */
	private static void addProjects() {

		RepositoryTypeDAO typeDAO = new RepositoryTypeDAO();
		Scanner scanner = new Scanner(System.in);

		System.out
				.println("WARNING: The CSV File must be separated by semmi-colon (;) and should contain the following columns: ");
		System.out
				.println("Project Name;Project Description;project website;repository link;Repository Type");
		System.out.println("WARNING: File must be in UTF-8 Format.");
		System.out.println();
		System.out.println("File Name (/home/user/Download/file.csv): ");
		String fileName = new String(scanner.nextLine());

		File file = new File(fileName);

		try {
			List<String> lines = Files.readAllLines(file.toPath(),
					Charsets.UTF_8);

			for (String line : lines) {
				Project project = new Project();
				Repository r = new Repository();
				String[] fields = line.split(";");
				project.setName(fields[0].trim());
				project.setSummary(fields[1].trim());
				project.setUrlsite(fields[2].trim());
				r.setUrladdress(fields[3].trim());
				r.setFolderName(fields[0].trim().replaceAll(" ", "-"));

				Repositorytype repositorytype = new Repositorytype();
				repositorytype.setName(fields[4].trim());
				repositorytype = typeDAO.select(repositorytype).get(0);

				addProject(project, r, repositorytype);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Projects added succesfully.");
		scanner.close();
	}

	/**
	 * Add many methods base on csv file.
	 */
	private static void addMethods() {

		Scanner scanner = new Scanner(System.in);

		System.out
				.println("WARNING: The CSV File must be separated by semmi-colon (;) and should contain the following columns: ");
		System.out.println("API Method,API Class");
		System.out
				.println("e.g.: com.hello.world.HelloWorld;doSomething(int,int,java.lang.String)");
		System.out.println("WARNING: File must be in UTF-8 Format.");
		System.out.println();
		System.out.println("File Name (/home/user/Download/file.csv): ");
		String fileName = new String(scanner.nextLine());

		File file = new File(fileName);

		try {
			List<String> lines = Files.readAllLines(file.toPath(),
					Charsets.UTF_8);

			for (String line : lines) {
				ApiClass apiClass = new ApiClass();
				ApiMethod apiMethod = new ApiMethod();
				String[] fields = line.split(";");
				apiClass.setName(fields[0]);
				apiMethod.setFqn(fields[1]);

				addApiMethod(apiMethod, apiClass);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Methods added succesfully.");

		scanner.close();
	}

	/**
	 * Add a project directly on the console.
	 */
	private static void addProjectByConsole() {

		Project p = new Project();
		Repository r = new Repository();
		Repositorytype repositorytype = new Repositorytype();

		Scanner scanner = new Scanner(System.in);

		System.out.println("Project name: ");
		p.setName(scanner.nextLine());
		System.out.println("Project Description: ");
		p.setSummary(scanner.nextLine());
		System.out.println("Project Website: ");
		p.setUrlsite(scanner.nextLine());
		System.out.println("Number of downloads at Market: ");
		p.setNumDownload(Long.parseLong(scanner.nextLine()));
		System.out.println("Repository link: ");
		r.setUrladdress(scanner.nextLine());
		System.out
				.println("Repository type (Subversion|Git|CVS|Bazaar|Mercurial|Compressed): ");
		repositorytype.setName(scanner.nextLine());
		r.setFolderName(p.getName().trim().replaceAll(" ", "-"));

		addProject(p, r, repositorytype);
		scanner.close();
	}

	/**
	 * Add an API method directly on the console.
	 */
	private static void addApiMethodByConsole() {

		ApiMethod m = new ApiMethod();
		ApiClass c = new ApiClass();

		Scanner scanner = new Scanner(System.in);

		System.out.println("Class name (e.g.: java.lang.String): ");
		c.setName(scanner.nextLine());
		System.out
				.println("Method name (e.g.: replace(java.lang.String,java.lang.String)): ");
		m.setFqn(scanner.nextLine());

		addApiMethod(m, c);

		scanner.close();
	}

	/**
	 * Add a project into the apiminer Project's database.
	 * 
	 * @param p
	 *            the project that will be added
	 * @param r
	 *            the project's repository that will be added
	 * @param repositorytype
	 *            the repository's type
	 */
	private static void addProject(Project p, Repository r,
			Repositorytype repositorytype) {
		RepositoryTypeDAO typeDAO = new RepositoryTypeDAO();
		repositorytype = typeDAO.select(repositorytype).get(0);
		r.setRepositorytype(repositorytype);
		p.setRepository(r);

		PreProcessing.processProject(p, new AndroidBuild());
	}

	/**
	 * Add an API method into the apiminer API's database.
	 * 
	 * @param m
	 *            api's method
	 * @param c
	 *            api's class
	 */
	private static void addApiMethod(ApiMethod m, ApiClass c) {
		ApiMethodDAO methodDAO = new ApiMethodDAO();
		ApiClassDAO classDAO = new ApiClassDAO();

		if (classDAO.select(c).size() == 0)
			classDAO.insert(c);
		c = classDAO.select(c).get(0);

		m.setApiClass(c);

		if (methodDAO.select(m).size() == 0)
			methodDAO.insert(m);
		else
			System.out.println("Method already exists.");
	}

}
