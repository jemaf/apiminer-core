/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.annotation.parser.JDTParser;
import br.ufmg.dcc.llp.apiminer.build.IBuild;
import br.ufmg.dcc.llp.apiminer.dao.ApiClassDAO;
import br.ufmg.dcc.llp.apiminer.dao.ExampleDAO;
import br.ufmg.dcc.llp.apiminer.dao.ModificationDAO;
import br.ufmg.dcc.llp.apiminer.dao.ProjectDAO;
import br.ufmg.dcc.llp.apiminer.dao.RepositoryDAO;
import br.ufmg.dcc.llp.apiminer.dao.RevisionDAO;
import br.ufmg.dcc.llp.apiminer.download.AbstractDownloader;
import br.ufmg.dcc.llp.apiminer.download.DownloaderFactory;
import br.ufmg.dcc.llp.apiminer.jpa.ApiClass;
import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Modification;
import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.jpa.Revision;
import br.ufmg.dcc.llp.apiminer.util.Util;
import br.ufmg.dcc.llp.ovisa.repository.model.service.IManterHistorico;
import br.ufmg.dcc.llp.ovisa.repository.model.service.impl.ManterHistoricoImpl;

/**
 * @author edumontandon
 * 
 */
public class PreProcessing {

	private static final Logger processingLog = LoggerFactory
			.getLogger(PreProcessing.class);

	public static final String PATH = Util.getRepositoryPath();

	public static final String CP = Util.getProjectsClasspath();

	/**
	 * Extract examples from project
	 * 
	 * @param p
	 */
	private static void extractExamplesFromProject(Project p) {
		processingLog.info("Extracting examples from system " + p.getName());
		ApiClassDAO apiClassDAO = new ApiClassDAO();
		List<ApiClass> classes = apiClassDAO.getAll();

		for (ApiClass apiClass : classes) {
			for (ApiMethod apiMethod : apiClass.getApiMethods()) {
				processingLog.info("Extracting examples of use of "
						+ apiClass.getName() + "." + apiMethod.getFqn());
				extractExampleOf(apiMethod, p);
			}
		}
	}

	/**
	 * extract examples from a specific method and project
	 * 
	 * @param method
	 * @param p
	 */
	private static void extractExampleOf(ApiMethod method, Project p) {

		JDTParser parser = new JDTParser(PATH, CP, method, p);
		parser.parse();

		// retrieve the list of modifications related to the project
		ExampleDAO exampleDAO = new ExampleDAO();
		ModificationDAO modificationDAO = new ModificationDAO();
		Modification modification = new Modification();
		RevisionDAO revisionDAO = new RevisionDAO();
		Revision r = new Revision();
		r.setProject(p);
		List<Revision> revisions = revisionDAO.select(r);
		List<Modification> modifications = new ArrayList<Modification>(); 
		
		for (Revision revision : revisions) {
			modification.setRevision(revision);
			modifications.addAll(modificationDAO.select(modification));
		}

		for (Example example : parser.getExamples()) {
			example.setApiMethod(method);
			example.setRepository(p.getRepository());

			// search for the example's artifact
			for (Modification mod : modifications) {
				if (mod.getArtifact()
						.getFilepath()
						.replace("\\", ".")
						.replace("/", ".")
						.contains(
								example.getFile().replace(File.separator, "."))) {
					example.setArtifact(mod.getArtifact());
				}
			}

			if (exampleDAO.select(example).size() == 0) {	
				
				int numFileCheckout = 0;
				for (Modification modification2 : modifications) {
					if(modification2.getArtifact().getId().compareTo(example.getArtifact().getId()) == 0)
						numFileCheckout++;
				}
				
				example.setNumFileCheckout(numFileCheckout);				
				exampleDAO.insert(example);
			}
		}
	}

	/**
	 * Process the project
	 * 
	 * @param p
	 *            the project that will be process
	 * @param buildTool
	 *            the interface that will build the project source code
	 * @return true if the project was processed successfully
	 */
	public static boolean processProject(Project p, IBuild buildTool) {

		// download the source files
		processingLog.info("Downloading project source code: " + p.getName());
		AbstractDownloader downloader = DownloaderFactory.getDownloader(p);
		boolean isDownloaded = downloader.downloadProject();

		if (!isDownloaded) {
			processingLog.warn("Project could not be downloaded: "
					+ p.getName());
			return false;
		} else
			processingLog.info("Project Downloaded: " + p.getName());

		// build
		boolean buildResult = buildProject(p, buildTool);
		if (!buildResult)
			return false;

		// if it is everything ok until now, insert the project on the
		// Database
		ProjectDAO projectDAO = new ProjectDAO();
		RepositoryDAO repositoryDAO = new RepositoryDAO();
		repositoryDAO.insert(p.getRepository());
		p.setRepository(repositoryDAO.select(p.getRepository()).get(0));
		projectDAO.insert(p);
		p = projectDAO.select(p).get(0);

		// extract the historical info
		extractProjectHistoricalInfo(p);

		// extract examples
		extractProjectExamples(p);

		processingLog.info("Pre-processing complete." + p.getName());
		return true;

	}

	/**
	 * Update all the information about a project
	 * 
	 * @param p
	 *            the project that will be updated
	 * @param buildTool
	 *            the builder interface that will build the project
	 */
	public static boolean updateProject(Project p, IBuild buildTool) {
		// download the source files
		processingLog.info("Downloading project source code: " + p.getName());
		AbstractDownloader downloader = DownloaderFactory.getDownloader(p);
		boolean isDownloaded = downloader.downloadProject();

		if (!isDownloaded) {
			processingLog.warn("Project could not be downloaded: "
					+ p.getName());
			return false;
		} else {
			processingLog.info("Project Downloaded: " + p.getName());
		}

		// build
		boolean buildResult = buildProject(p, buildTool);
		if (!buildResult)
			return false;

		// extract historical info
		extractProjectHistoricalInfo(p);

		// extract project examples
		extractProjectExamples(p);

		processingLog.info("Updating complete: " + p.getName());
		return true;
	}

	/**
	 * Extract the project's example
	 * 
	 * @param p
	 *            the selected project
	 */
	private static void extractProjectExamples(Project p) {
		processingLog.info("Extracting project's examples: " + p.getName());
		extractExamplesFromProject(p);
	}

	/**
	 * Extract the project's historical info
	 * 
	 * @param p
	 *            the selected project
	 */
	private static void extractProjectHistoricalInfo(Project p) {
		processingLog.info("Extracting project's historical info: "
				+ p.getName());
		extractHistoricalInfo(p);
	}

	/**
	 * Build a project based on the build interface
	 * 
	 * @param p
	 *            the project that will be builded
	 * @param buildTool
	 *            the interface that will build the project
	 */
	private static boolean buildProject(Project p, IBuild buildTool) {
		if (buildTool != null) {
			processingLog.info("Building project: " + p.getName());
			String systemFolder = p.getRepository().getFolderName();
			boolean result = buildTool.build(PreProcessing.PATH
					+ File.separator + systemFolder);

			if (!result) {
				processingLog.warn("Project could not be builded: "
						+ p.getName());
				return false;
			} else {
				processingLog.info("Project builded: " + p.getName());
				return true;
			}
		} else {
			processingLog.warn("Incorrect Build Interface was provided.");
			return false;
		}
	}

	/**
	 * Extract the historical info of a project
	 * 
	 * @param p
	 *            the project where the info will be extracted
	 */
	private static void extractHistoricalInfo(Project p) {
		// TODO: This is temporary
		if (p.getRepository().getRepositorytype().getName().toLowerCase()
				.contains("subversion")) {
			try {
				IManterHistorico manterHistorico = new ManterHistoricoImpl();
				manterHistorico.atualizar(p.getId());
			} catch (Exception e) {
				processingLog.error("Error during the historical extraction: ",
						e);
			}
		}
	}
}
