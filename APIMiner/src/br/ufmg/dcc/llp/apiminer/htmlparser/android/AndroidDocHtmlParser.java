/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.htmlparser.android;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.OutputDocument;
import net.htmlparser.jericho.Source;
import br.ufmg.dcc.llp.apiminer.dao.ApiClassDAO;
import br.ufmg.dcc.llp.apiminer.dao.ApiMethodDAO;
import br.ufmg.dcc.llp.apiminer.dao.ExampleDAO;
import br.ufmg.dcc.llp.apiminer.htmlparser.IHtmlParser;
import br.ufmg.dcc.llp.apiminer.jpa.ApiClass;
import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.util.Util;

/**
 * @author edumontandon
 * 
 */
public class AndroidDocHtmlParser implements IHtmlParser {

	private static final String ACTION = "<td align=\"center\"><form method=\"get\" action=\"/apiminer/ExampleServlet\" >"
			+ "<input type=\"hidden\" name=\"method\" value=\"$METHOD\" />"
			+ "<input type=\"submit\" value=\"Example\" />"
			+ "</form> <font size=\"1\"> $NUMBER Examples</font></td>";

	List<Example> examples = null;

	public AndroidDocHtmlParser() {
		ExampleDAO exampleDAO = new ExampleDAO();
		examples = exampleDAO.getAll();
	}
	
	@Override
	public void parse(String htmlFiles[], String outPath, String extension) {

		boolean canSave = false;

		try {
			for (String sourceUrlString : htmlFiles) {
				System.out.println("Processing file: " + sourceUrlString);

				// parse the file and get all the tables of it
				Source source = new Source(new URL("file:" + sourceUrlString));
				List<Element> tableList = source.getAllElements(HTMLElementName.TABLE);
				OutputDocument doc = new OutputDocument(source);

				for (Element table : tableList) {

					// check if the table is that what I want
					if (table.getAttributeValue("id") != null
							&& table.getAttributeValue("id").equals("pubmethods")) {

						// iterate over the rows of the table
						List<Element> trList = table.getAllElements(HTMLElementName.TR);
						for (Element tr : trList) {
							if (tr.getChildElements().size() == 2) {

								// recover the links of the row and replace it
								// based on the mask
								List<Element> as = tr.getAllElements(HTMLElementName.A);
								String methodName = "";
								String className = "";
								String trButton = ACTION;
								for (Element a : as) {
									if (a.getAttributeValue("href") != null
											&& a.getAttributeValue("href").contains("#")) {
										className = Util
												.strToHtml(a.getAttributeValue("href").split(".html#")[0])
												.split("../reference/")[1].replace("/", ".");
										methodName = Util
												.strToHtml(a.getAttributeValue("href").split("#")[1]);
										trButton = trButton.replace("$METHOD", className + "." + methodName);
										trButton = trButton.replace("$NUMBER",
												String.valueOf(getNumberOfMethod(className, methodName)));
										canSave = true;
									}
								}

								// inject the button at the row
								List<Element> tds = tr.getChildElements();
								Source btnSrc = new Source(trButton);
								tds.add(0, btnSrc.getFirstElement());
								String tdsStr = "";
								for (Element td : tds)
									tdsStr += td.toString();
								String newTr = tr.getStartTag().toString() + tdsStr + tr.getEndTag();
								doc.replace(tr, newTr);
							}
						}
					}
				}

				if (canSave) {
					String fileName = sourceUrlString.substring(sourceUrlString.lastIndexOf("/"),
							sourceUrlString.length());
					fileName = fileName.substring(0, fileName.lastIndexOf("."));
					String filePath = outPath
							+ sourceUrlString.substring(sourceUrlString.lastIndexOf("docs") + 4,
									sourceUrlString.lastIndexOf(fileName));
					File newFile = new File(filePath);
					newFile.mkdirs();
					// Create file
					FileWriter fstream = new FileWriter(filePath + fileName + extension);
					BufferedWriter out = new BufferedWriter(fstream);
					System.out.println("Writing to file");
					out.write(doc.toString());
					// Close the output stream
					out.close();
				}

				canSave = false;
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * @param className
	 * @param methodName
	 * @return
	 */
	private int getNumberOfMethod(String className, String methodName) {
		int numberOfOccurrence = 0;
		className = className.trim();
		methodName = methodName.trim();

		for (Example example : examples) {

			if (example.getApiMethod().getFqn().equals(methodName)
					&& example.getApiMethod().getApiClass().getName().equals(className))
				numberOfOccurrence++;
		}

		return numberOfOccurrence;
	}

	/**
	 * Collect the methods of a list of android document files and insert it on
	 * the database
	 * 
	 * @param files
	 */
	public void collectMethodsOfFile(String[] files) {

		ApiClassDAO classDAO = new ApiClassDAO();
		ApiMethodDAO methodDAO = new ApiMethodDAO();

		try {
			for (String sourceUrlString : files) {
				System.out.println("Processing file: " + sourceUrlString);

				// parse the file and get all the tables of it
				Source source = new Source(new URL("file:" + sourceUrlString));
				List<Element> tableList = source.getAllElements(HTMLElementName.TABLE);

				String className = sourceUrlString
						.substring(
								sourceUrlString.lastIndexOf(File.separator + "reference" + File.separator),
								sourceUrlString.length()).replace(".html", "")
						.replace(File.separator + "reference" + File.separator, "")
						.replace(File.separator, ".");

				ApiClass clazz = new ApiClass();
				clazz.setName(className);
				classDAO.insert(clazz);
				clazz = classDAO.select(clazz).get(0);

				for (Element table : tableList) {

					if (table.getAttributeValue("id") != null
							&& table.getAttributeValue("id").equals("pubmethods")) {

						List<Element> trList = table.getAllElements(HTMLElementName.TR);
						for (Element tr : trList) {
							if (tr.getChildElements().size() == 2) {
								Element span = tr.getFirstElement(HTMLElementName.SPAN);
								Element a = span.getFirstElement(HTMLElementName.A);
//								List<Element> as = tr.getAllElements(HTMLElementName.A);
//								for (Element a : as) {
									if (a.getAttributeValue("href") != null
											&& a.getAttributeValue("href").contains("#")
											&& a.getAttributeValue("href").contains("(")
											&& a.getAttributeValue("href").contains(")")) {
										String methodName = Util.htmlToStr(a.getAttributeValue("href").split(
												"#")[1].replaceAll(", ", ",").trim());

										ApiMethod method = new ApiMethod();
										method.setFqn(methodName);
										method.setApiClass(clazz);
										methodDAO.insert(method);

									}
//								}
							}
						}
					}
				}
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
}
