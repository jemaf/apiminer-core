/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.htmlparser;

/**
 * @author edumontandon
 *
 */
public interface IHtmlParser {

	/**
     * @param htmlFiles
     * @param outPath
     */
    public void parse(String[] htmlFiles, String outPath, String extension);
    

}
