/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.htmlparser.main;

import java.util.ArrayList;
import java.util.Map;

import br.ufmg.dcc.llp.apiminer.htmlparser.android.AndroidDocHtmlParser;
import br.ufmg.dcc.llp.apiminer.util.Util;

/**
 * @author edumontandon
 * 
 */
public class AndroidDocMain {

	public static void main(String args[]) {
		 Map<String, ArrayList<String>> params = Util.handleEntries(args);
		
		 ArrayList<String> files = params.get("-files");
		 String outDir = params.get("-out").get(0);
		 String extension = params.get("-extension").get(0);
		
		 System.out.println("Parsing Android Site");
		 ArrayList<String> temp = new ArrayList<String>();
		
		 for (String file : files)
		 temp.addAll(Util.collectFiles(file, ".html"));
		
		 AndroidDocHtmlParser andParser = new AndroidDocHtmlParser();
		 andParser.parse(temp.toArray(new String[temp.size()]), outDir,
		 extension);
		 System.out.println("Android Site Parsed");

	}
}
