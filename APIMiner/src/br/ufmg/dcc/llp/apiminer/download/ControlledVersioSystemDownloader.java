package br.ufmg.dcc.llp.apiminer.download;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.jpa.Repository;
import br.ufmg.dcc.llp.apiminer.util.Util;

public class ControlledVersioSystemDownloader extends AbstractDownloader {

	private static final Logger log = LoggerFactory.getLogger(ControlledVersioSystemDownloader.class);

	public ControlledVersioSystemDownloader(Project p) {
		super(p);
	}

	@Override
	public boolean downloadProject() {

		Repository r = super.project.getRepository();
		String repositoryType = super.project.getRepository().getRepositorytype().getName();
		String repExec = Util.getRepositoryExecutablePath(repositoryType);

		if (repositoryType.equalsIgnoreCase("subversion")) {
			log.debug("Downloading svn project");
			return Util.executeInTerminal(repExec + " co " + r.getUrladdress() + " " + PATH + File.separator
					+ r.getFolderName());
		} else if (repositoryType.equalsIgnoreCase("mercurial")) {
			log.debug("Downloading hg project");
			return Util.executeInTerminal(repExec + " clone " + r.getUrladdress() + " " + PATH
					+ File.separator + r.getFolderName());
		} else if (repositoryType.equalsIgnoreCase("cvs")) {
			log.debug("Downloading cvs project");
			return Util.executeInTerminal(repExec + " co -P " + r.getUrladdress() + " " + PATH
					+ File.separator + r.getFolderName());
		} else if (repositoryType.equalsIgnoreCase("git")) {
			log.debug("Downloading git project");
			return Util.executeInTerminal(repExec + " clone " + r.getUrladdress() + " " + PATH
					+ File.separator + r.getFolderName());
		} else if (repositoryType.equalsIgnoreCase("bazaar")) {
			log.debug("Downloading bzr project");
			return Util.executeInTerminal(repExec + " checkout " + r.getUrladdress() + " " + PATH
					+ File.separator + r.getFolderName());
		}

		return false;
	}

}
