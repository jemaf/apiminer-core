package br.ufmg.dcc.llp.apiminer.download;

import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.util.Util;

public abstract class AbstractDownloader {

	protected static final String PATH = Util.getRepositoryPath();
	
	protected Project project;
	
	public AbstractDownloader(Project p) {
		this.project = p;
	}
	
	public abstract boolean downloadProject();
	
}
