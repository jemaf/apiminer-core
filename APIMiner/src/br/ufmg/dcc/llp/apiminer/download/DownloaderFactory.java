package br.ufmg.dcc.llp.apiminer.download;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.jpa.Project;

public final class DownloaderFactory {

	private static Logger log = LoggerFactory
			.getLogger(DownloaderFactory.class);

	public static AbstractDownloader getDownloader(Project p) {

		if (p.getRepository().getRepositorytype() == null) {
			log.warn("Project without source code repository: " + p.getName());
			return null;
		}

		if (p.getRepository().getRepositorytype().getName()
				.equalsIgnoreCase("compressed"))
			return new HttpDownloader(p);
		else
			return new ControlledVersioSystemDownloader(p);
	}

}
