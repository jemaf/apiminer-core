package br.ufmg.dcc.llp.apiminer.download;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.jpa.Project;

public class HttpDownloader extends AbstractDownloader {

	private static final Logger log = LoggerFactory
			.getLogger(HttpDownloader.class);

	public HttpDownloader(Project p) {
		super(p);
	}

	@Override
	public boolean downloadProject() {
		log.info("Downloading the project's compressed file");
		
		try {
			// retrieve the information of the project that will be extracted
			String compressedProjectLink = project.getRepository()
					.getUrladdress();
			String projectPath = PATH + project.getRepository().getFolderName();
			String projectFileName = compressedProjectLink.split("/")[compressedProjectLink
					.split("/").length - 1];

			// retrieve the compressed file
			BufferedInputStream bis = new BufferedInputStream(new URL(
					compressedProjectLink).openStream());

			InputStream temp = null;

			// test if the file is double compressed
			if (projectFileName.endsWith(".tar.gz")) {
				projectFileName = projectFileName.replace(".gz", "");
				temp = new GzipCompressorInputStream(bis);

			} else if (projectFileName.endsWith(".tar.bz2")) {
				projectFileName = projectFileName.replace(".bz2", "");
				temp = new BZip2CompressorInputStream(bis);
			}

			// extract the double compressed file
			if (temp != null) {
				FileOutputStream out = new FileOutputStream(projectPath
						+ projectFileName);
				IOUtils.copy(temp, out);
				out.close();
				temp.close();

				bis = new BufferedInputStream(new FileInputStream(projectPath
						+ projectFileName));
			}

			// extract the compressed file
			ArchiveInputStream ais = new ArchiveStreamFactory()
					.createArchiveInputStream(bis);
			ArchiveEntry entry = null;

			while ((entry = ais.getNextEntry()) != null) {

				File f = new File(projectPath, entry.getName());
				if (entry.isDirectory())
					f.mkdir();
				else {
					if (!f.exists())
						new File(f.getAbsolutePath().replace(f.getName(), ""))
								.mkdirs();
					OutputStream out = new FileOutputStream(f);
					IOUtils.copy(ais, out);
					out.close();
				}
			}
			ais.close();

			// delete the double compressed file (if it exists)
			if (temp != null)
				new File(projectPath + projectFileName).delete();

			return true;
		} catch (Exception e) {
			log.error("Problem during the downloading process.", e);
		}

		return false;
	}
	public static void main(String args[]) {

		HttpDownloader downloader = new HttpDownloader(new Project());
		downloader.downloadProject();

	}

}
