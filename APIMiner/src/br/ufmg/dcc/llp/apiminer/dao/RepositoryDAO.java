/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Repository;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class RepositoryDAO extends AbstractDAO<Repository>{

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Repository> select(Repository object) {
		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Repository> q = cb.createQuery(Repository.class);
		Root<Repository> from = q.from(Repository.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getFolderName() != null)
			predicates.add(cb.equal(from.get("folderName"), object.getFolderName()));
		if (object.getPasswd() != null)
			predicates.add(cb.equal(from.get("passwd"), object.getPasswd()));
		if (object.getRepositorytype() != null)
			predicates.add(cb.equal(from.get("repositorytype"), object.getRepositorytype()));
		if (object.getUrladdress() != null)
			predicates.add(cb.equal(from.get("urladdress"), object.getUrladdress()));
		if (object.getUsername() != null)
			predicates.add(cb.equal(from.get("username"), object.getUsername()));
		

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Repository> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    }

}
