/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.ufmg.dcc.llp.apiminer.jpa.IEntity;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 * 
 */
public abstract class AbstractDAO<Type extends IEntity> {

	@SuppressWarnings("unchecked")
	private Class<Type> getEntityClass() {
		return (Class<Type>) ((ParameterizedType) getClass().getGenericSuperclass())
		        .getActualTypeArguments()[0];
	}

	public List<Type> getAll() {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Type> q = cb.createQuery(getEntityClass());
		q.from(getEntityClass());

		return em.createQuery(q).getResultList();
	}

	public abstract List<Type> select(Type object);

//	public abstract boolean save(Type object);
	
	public boolean update(Type object) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		try {
			em.getTransaction().begin();
			em.merge(object);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
		return true;

	}

	public boolean insert(Type object) {

		EntityManager em = DataBase.getFactory().createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
		return true;
	}

	public boolean delete(Type object) {

		EntityManager em = DataBase.getFactory().createEntityManager();
		try {
			em.getTransaction().begin();
			Type deletedClass = em.find(getEntityClass(), object.getId());
			em.remove(deletedClass);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
		return true;
	}

}
