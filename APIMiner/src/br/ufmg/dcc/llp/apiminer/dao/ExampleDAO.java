/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 * 
 */
public class ExampleDAO extends AbstractDAO<Example>{

	public List<Example> select(Example ex) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Example> q = cb.createQuery(Example.class);
		Root<Example> from = q.from(Example.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (ex.getId() != null)
			predicates.add(cb.equal(from.get("id"), ex.getId()));
		if (ex.getApiMethod() != null)
			predicates.add(cb.equal(from.get("apiMethod"), ex.getApiMethod()));
		if (ex.getExample() != null)
			predicates.add(cb.equal(from.get("example"), ex.getExample()));
		if (ex.getFile() != null)
			predicates.add(cb.equal(from.get("file"), ex.getFile()));
		if (ex.getNumFileCheckout() != null)
			predicates.add(cb.equal(from.get("numFileCheckout"), ex.getNumFileCheckout()));
		if (ex.getRepository() != null)
			predicates.add(cb.equal(from.get("repository"), ex.getRepository()));
		if (ex.getSeed() != null)
			predicates.add( cb.equal(from.get("seed"), ex.getSeed()));
		if (ex.getSourceMethod() != null)
			predicates.add(cb.equal(from.get("sourceMethod"), ex.getSourceMethod()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		
		q.orderBy(cb.desc(from.get("numFileCheckout")));
		
		List<Example> result = em.createQuery(q).getResultList();
		em.close();
		return result;
	}
}
