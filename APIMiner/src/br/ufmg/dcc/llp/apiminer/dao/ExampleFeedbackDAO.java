package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.ExampleFeedback;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

public class ExampleFeedbackDAO extends AbstractDAO<ExampleFeedback> {

	@Override
	public List<ExampleFeedback> select(ExampleFeedback exampleFeedback) {
		
		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ExampleFeedback> q = cb.createQuery(ExampleFeedback.class);
		Root<ExampleFeedback> from = q.from(ExampleFeedback.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (exampleFeedback.getId() != null)
			predicates.add(cb.equal(from.get("id"), exampleFeedback.getId()));
		if (exampleFeedback.getComment() != null)
			predicates.add(cb.equal(from.get("comment"), exampleFeedback.getComment()));
		if(exampleFeedback.getExample() != null)
			predicates.add(cb.equal(from.get("example"), exampleFeedback.getExample()));
		if(exampleFeedback.getRating() != null)
			predicates.add(cb.equal(from.get("rating"), exampleFeedback.getRating()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		
		q.orderBy(cb.desc(from.get("numFileCheckout")));
		
		List<ExampleFeedback> result = em.createQuery(q).getResultList();
		em.close();
		return result;
	}

}
