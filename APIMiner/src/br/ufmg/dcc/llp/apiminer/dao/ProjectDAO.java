/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class ProjectDAO extends AbstractDAO<Project> {

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Project> select(Project object) {
		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Project> q = cb.createQuery(Project.class);
		Root<Project> from = q.from(Project.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getAdditionalcomments() != null)
			predicates.add(cb.equal(from.get("additionalcomments"), object.getAdditionalcomments()));
		if (object.getName() != null)
			predicates.add(cb.equal(from.get("name"), object.getName()));
		if (object.getRepository() != null)
			predicates.add(cb.equal(from.get("repository"), object.getRepository()));
		if (object.getSummary() != null)
			predicates.add(cb.equal(from.get("summary"), object.getSummary()));
		if (object.getUrlsite() != null)
			predicates.add(cb.equal(from.get("urlsite"), object.getUrlsite()));
		if(object.getNumDownload() != null)
			predicates.add(cb.equal(from.get("numDownload"), object.getNumDownload()));
		

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Project> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    	
    }

}
