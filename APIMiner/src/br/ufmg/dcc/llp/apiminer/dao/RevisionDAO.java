/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Revision;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class RevisionDAO extends AbstractDAO<Revision>{

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Revision> select(Revision object) {
		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Revision> q = cb.createQuery(Revision.class);
		Root<Revision> from = q.from(Revision.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getAuthor() != null)
			predicates.add(cb.equal(from.get("author"), object.getAuthor()));
		if (object.getDatecommit() != null)
			predicates.add(cb.equal(from.get("datecommit"), object.getDatecommit()));
		if (object.getMessage() != null)
			predicates.add(cb.equal(from.get("message"), object.getMessage()));
		if (object.getProject() != null)
			predicates.add(cb.equal(from.get("project"), object.getProject()));
		if (object.getRevisionorder() != null)
			predicates.add(cb.equal(from.get("revisionorder"), object.getRevisionorder()));
		if (object.getTimeins() != null)
			predicates.add(cb.equal(from.get("timeins"), object.getTimeins()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Revision> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    	
    }
}
