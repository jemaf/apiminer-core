/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.ApiClass;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 * 
 */
public class ApiClassDAO extends AbstractDAO<ApiClass>{

	public List<ApiClass> select(ApiClass clazz) {
		
		EntityManager em = DataBase.getFactory().createEntityManager();
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ApiClass> q = cb.createQuery(ApiClass.class);
		Root<ApiClass> from = q.from(ApiClass.class);
		
		ArrayList<Predicate> predicates = new ArrayList<Predicate>();
		
		if(clazz.getId() != null)
		predicates.add(cb.equal(from.get("id"), clazz.getId()));
		if(clazz.getName() != null)
		predicates.add(cb.equal(from.get("name"), clazz.getName()));
		if(clazz.getDescription() != null)
		predicates.add(cb.equal(from.get("description"), clazz.getDescription()));
		
		if(predicates.size() > 0)
		q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		
		List<ApiClass> result = em.createQuery(q).getResultList();
		em.close();
		return result;
	}
}
