/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.IEntity;
import br.ufmg.dcc.llp.apiminer.jpa.Repositorytype;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class RepositoryTypeDAO extends AbstractDAO<Repositorytype> {

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Repositorytype> select(Repositorytype object) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Repositorytype> q = cb.createQuery(Repositorytype.class);
		Root<Repositorytype> from = q.from(Repositorytype.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getName() != null)
			predicates.add(cb.equal(from.get("name"), object.getName()));
	
		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Repositorytype> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    	
    }

}
