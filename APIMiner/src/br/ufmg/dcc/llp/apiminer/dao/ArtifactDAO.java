/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Artifact;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class ArtifactDAO extends AbstractDAO<Artifact> {

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Artifact> select(Artifact object) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Artifact> q = cb.createQuery(Artifact.class);
		Root<Artifact> from = q.from(Artifact.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getFilepath() != null)
			predicates.add(cb.equal(from.get("filepath"), object.getFilepath()));
		if (object.getTimeins() != null)
			predicates.add(cb.equal(from.get("timeins"), object.getTimeins()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Artifact> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    }

}
