/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 * 
 */
public class ApiMethodDAO extends AbstractDAO<ApiMethod>{

	public List<ApiMethod> select(ApiMethod method) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ApiMethod> q = cb.createQuery(ApiMethod.class);
		Root<ApiMethod> from = q.from(ApiMethod.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		if (method.getId() != null)
			predicates.add(cb.equal(from.get("id"), method.getId()));
		if (method.getFqn() != null)
			predicates.add(cb.equal(from.get("fqn"), method.getFqn()));
		if (method.getApiClass() != null)
			predicates.add(cb.equal(from.get("apiClass"), method.getApiClass()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<ApiMethod> result = em.createQuery(q).getResultList();
		em.close();
		return result;
	}
}
