/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Modification;
import br.ufmg.dcc.llp.apiminer.jpa.conf.DataBase;

/**
 * @author edumontandon
 *
 */
public class ModificationDAO extends AbstractDAO<Modification> {

	/* (non-Javadoc)
     * @see br.ufmg.dcc.llp.apiminer.controller.Controller#select(br.ufmg.dcc.llp.apiminer.jpa.IEntity)
     */
    @Override
    public List<Modification> select(Modification object) {

		EntityManager em = DataBase.getFactory().createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Modification> q = cb.createQuery(Modification.class);
		Root<Modification> from = q.from(Modification.class);

		ArrayList<Predicate> predicates = new ArrayList<Predicate>();

		if (object.getId() != null)
			predicates.add(cb.equal(from.get("id"), object.getId()));
		if (object.getActiontype() != null)
			predicates.add(cb.equal(from.get("actiontype"), object.getActiontype()));
		if (object.getTimeins() != null)
			predicates.add(cb.equal(from.get("timeins"), object.getTimeins()));
		if (object.getArtifact() != null)
			predicates.add(cb.equal(from.get("artifact"), object.getArtifact()));
		if(object.getRevision() != null)
			predicates.add(cb.equal(from.get("revision"), object.getRevision()));

		if (predicates.size() > 0)
			q.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		List<Modification> result = em.createQuery(q).getResultList();
		em.close();
		return result;
    
    }

}
