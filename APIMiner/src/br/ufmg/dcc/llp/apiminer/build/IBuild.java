package br.ufmg.dcc.llp.apiminer.build;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 */

/**
 * @author edumontandon
 *
 */
public interface IBuild {

	static Logger buildLogging = LoggerFactory.getLogger(IBuild.class);
	
	public boolean build(String path);
	
}
