package br.ufmg.dcc.llp.apiminer.build.android;

import java.io.File;
import java.util.ArrayList;

import br.ufmg.dcc.llp.apiminer.build.IBuild;
import br.ufmg.dcc.llp.apiminer.util.Util;

/**
 * 
 */

/**
 * @author edumontandon
 * 
 */
public class AndroidBuild implements IBuild {

	private static String ANDROID_VERSION_PATH = "/home/apiminer/android/platforms/android-16/android.jar";

	private static final String AIDL = "/home/apiminer/android/platform-tools//aidl";

	private static final String AAPT = "/home/apiminer/android/platform-tools//aapt";

	/**
	 * build the manifest files to generate the R.java file
	 * 
	 * @param manifestFiles
	 *            list of manifest files that will be build
	 * @return true if it build correctly, false otherwise
	 */
	private boolean buildAapt(ArrayList<String> manifestFiles) {

		// aapt package -v -f -m -S res -J src -M ./AndroidManifest.xml -I
		// /Developer/Android/android-sdk/platforms/android-Honeycomb/android.jar
		buildLogging.debug("Compiling Manifest files...");

		if (manifestFiles.size() == 0) {
			buildLogging.debug("No manifest files to compile");
			return true;
		}

		for (String manifest : manifestFiles) {
			String currentDir = manifest.substring(0,
					manifest.lastIndexOf(File.separator));
			if (!currentDir.toLowerCase().matches(
					".*" + File.separator + "test[s]?[" + File.separator
							+ "]?.*")) {
				boolean result = Util.executeInTerminal(AAPT
						+ " package -v -f -m -S " + currentDir + File.separator
						+ "res -J " + currentDir + File.separator + "src -M "
						+ manifest + " -I " + ANDROID_VERSION_PATH);

				if (!result)
					return false;
			}
		}

		return true;
	}

	/**
	 * Generate the java files from aidl files
	 * 
	 * @param aidlFiles
	 *            list of aidl files that will be generated
	 * @return true if it builds correctly, false otherwise
	 */
	private boolean buildAidl(ArrayList<String> aidlFiles) {
		// aidl -Isrc/ $file
		buildLogging.debug("Compiling aidl files...");

		if (aidlFiles.size() == 0) {
			buildLogging.debug("No aidl Files to compile");
			return true;
		}

		for (String aidlFile : aidlFiles) {
			if (aidlFile.lastIndexOf("src") != -1) {
				String currentDir = aidlFile.substring(0,
						aidlFile.lastIndexOf("src"));
				boolean result = Util.executeInTerminal(AIDL + " -I"
						+ currentDir + "src" + File.separator + " " + aidlFile);
				if (!result)
					return false;
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.br.ufmg.apiminer.build.IBuild#build(java.lang.String)
	 */
	public boolean build(String path) {
		buildLogging.info("Compiling " + path);

		ArrayList<String> aidlFiles = Util.collectFiles(path, ".aidl");
		ArrayList<String> manifestFiles = Util.collectFiles(path,
				"AndroidManifest.xml");

		boolean aidlCompleted = this.buildAidl(aidlFiles);
		boolean aaptCompleted = this.buildAapt(manifestFiles);

		return aidlCompleted && aaptCompleted;
	}
}
