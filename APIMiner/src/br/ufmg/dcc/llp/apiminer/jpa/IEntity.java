/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.jpa;

/**
 * @author edumontandon
 *
 */
public interface IEntity {

	public Long getId();
	
}
