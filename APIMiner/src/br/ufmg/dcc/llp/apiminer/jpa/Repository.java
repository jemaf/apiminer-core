package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the repository database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Repository")
public class Repository implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="folder_name")
	private String folderName;

	private String passwd;

	private String urladdress;

	private String username;

	//bi-directional many-to-one association to Example
	@OneToMany(mappedBy="repository")
	private List<Example> examples;

	//bi-directional many-to-one association to Project
	@OneToMany(mappedBy="repository")
	private List<Project> projects;

	//bi-directional many-to-one association to Repositorytype
	@ManyToOne
	private Repositorytype repositorytype;

	public Repository() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFolderName() {
		return this.folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getUrladdress() {
		return this.urladdress;
	}

	public void setUrladdress(String urladdress) {
		this.urladdress = urladdress;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Example> getExamples() {
		return this.examples;
	}

	public void setExamples(List<Example> examples) {
		this.examples = examples;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Repositorytype getRepositorytype() {
		return this.repositorytype;
	}

	public void setRepositorytype(Repositorytype repositorytype) {
		this.repositorytype = repositorytype;
	}

}