package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the example database table.
 * 
 */
@Entity
public class Example implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String example;

	private String file;

	@Column(name="num_file_checkout")
	private Integer numFileCheckout;

	private String seed;

	@Column(name="source_method")
	private String sourceMethod;
	
	@Lob
	private byte[] sourceFile;

	//bi-directional many-to-one association to ApiMethod
	@ManyToOne
	@JoinColumn(name="api_method_id")
	private ApiMethod apiMethod;

	//bi-directional many-to-one association to Artifact
	@ManyToOne
	private Artifact artifact;

	//bi-directional many-to-one association to Repository
	@ManyToOne
	private Repository repository;

	//bi-directional many-to-one association to ExampleFeedback
	@OneToMany(mappedBy="example")
	private List<ExampleFeedback> exampleFeedbacks;

	public Example() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExample() {
		return this.example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Integer getNumFileCheckout() {
		return this.numFileCheckout;
	}

	public void setNumFileCheckout(Integer numFileCheckout) {
		this.numFileCheckout = numFileCheckout;
	}

	public String getSeed() {
		return this.seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public String getSourceMethod() {
		return this.sourceMethod;
	}

	public void setSourceMethod(String sourceMethod) {
		this.sourceMethod = sourceMethod;
	}

	public ApiMethod getApiMethod() {
		return this.apiMethod;
	}

	public void setApiMethod(ApiMethod apiMethod) {
		this.apiMethod = apiMethod;
	}

	public Artifact getArtifact() {
		return this.artifact;
	}

	public void setArtifact(Artifact artifact) {
		this.artifact = artifact;
	}

	public Repository getRepository() {
		return this.repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public List<ExampleFeedback> getExampleFeedbacks() {
		return this.exampleFeedbacks;
	}

	public void setExampleFeedbacks(List<ExampleFeedback> exampleFeedbacks) {
		this.exampleFeedbacks = exampleFeedbacks;
	}

	public String getSourceFile() {
		return new String(sourceFile);
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile.getBytes();
	}
}