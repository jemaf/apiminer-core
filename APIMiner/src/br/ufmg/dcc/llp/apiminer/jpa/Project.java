package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the project database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Project")
public class Project implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String additionalcomments;

	private String name;

	private String summary;

	private String urlsite;

	//bi-directional many-to-one association to Repository
	@ManyToOne
	private Repository repository;

	//bi-directional many-to-one association to Revision
	@OneToMany(mappedBy="project")
	private List<Revision> revisions;

	@Column(name="num_download")
	private Long numDownload;
	
	public Project() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdditionalcomments() {
		return this.additionalcomments;
	}

	public void setAdditionalcomments(String additionalcomments) {
		this.additionalcomments = additionalcomments;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getUrlsite() {
		return this.urlsite;
	}

	public void setUrlsite(String urlsite) {
		this.urlsite = urlsite;
	}

	public Repository getRepository() {
		return this.repository;
	}
	
	public Long getNumDownload() {
		return this.numDownload;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public List<Revision> getRevisions() {
		return this.revisions;
	}

	public void setRevisions(List<Revision> revisions) {
		this.revisions = revisions;
	}
	
	public void setNumDownload(Long numDownload) {
		this.numDownload = numDownload;
	}

}