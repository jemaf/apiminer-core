package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the repositorytype database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Repositorytype")
public class Repositorytype implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String name;

	//bi-directional many-to-one association to Repository
	@OneToMany(mappedBy="repositorytype")
	private List<Repository> repositories;

	public Repositorytype() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Repository> getRepositories() {
		return this.repositories;
	}

	public void setRepositories(List<Repository> repositories) {
		this.repositories = repositories;
	}

}