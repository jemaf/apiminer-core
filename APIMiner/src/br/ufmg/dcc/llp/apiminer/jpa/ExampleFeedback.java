package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the example_feedback database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.ExampleFeedback")
@Table(name="example_feedback")
public class ExampleFeedback implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String comment;

	private Integer rating;

	//bi-directional many-to-one association to Example
	@ManyToOne
	private Example example;

	public ExampleFeedback() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getRating() {
		return this.rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Example getExample() {
		return this.example;
	}

	public void setExample(Example example) {
		this.example = example;
	}

}