package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the modification database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Modification")
public class Modification implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String actiontype;

	private Timestamp timeins;

	//bi-directional many-to-one association to Artifact
	@ManyToOne
	private Artifact artifact;

	//bi-directional many-to-one association to Revision
	@ManyToOne
	private Revision revision;

	public Modification() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActiontype() {
		return this.actiontype;
	}

	public void setActiontype(String actiontype) {
		this.actiontype = actiontype;
	}

	public Timestamp getTimeins() {
		return this.timeins;
	}

	public void setTimeins(Timestamp timeins) {
		this.timeins = timeins;
	}

	public Artifact getArtifact() {
		return this.artifact;
	}

	public void setArtifact(Artifact artifact) {
		this.artifact = artifact;
	}

	public Revision getRevision() {
		return this.revision;
	}

	public void setRevision(Revision revision) {
		this.revision = revision;
	}

}