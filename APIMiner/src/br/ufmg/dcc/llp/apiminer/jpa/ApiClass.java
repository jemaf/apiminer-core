package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the api_class database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.ApiClass")
@Table(name="api_class")
public class ApiClass implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String description;

	private String name;

	//bi-directional many-to-one association to ApiMethod
	@OneToMany(mappedBy="apiClass")
	private List<ApiMethod> apiMethods;

	public ApiClass() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ApiMethod> getApiMethods() {
		return this.apiMethods;
	}

	public void setApiMethods(List<ApiMethod> apiMethods) {
		this.apiMethods = apiMethods;
	}

}