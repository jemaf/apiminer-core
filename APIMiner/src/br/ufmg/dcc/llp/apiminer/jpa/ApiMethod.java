package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the api_method database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.ApiMethod")
@Table(name="api_method")
public class ApiMethod implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String fqn;

	//bi-directional many-to-one association to ApiClass
	@ManyToOne
	@JoinColumn(name="api_class_id")
	private ApiClass apiClass;

	//bi-directional many-to-one association to Example
	@OneToMany(mappedBy="apiMethod")
	private List<Example> examples;

	public ApiMethod() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFqn() {
		return this.fqn;
	}

	public void setFqn(String fqn) {
		this.fqn = fqn;
	}

	public ApiClass getApiClass() {
		return this.apiClass;
	}

	public void setApiClass(ApiClass apiClass) {
		this.apiClass = apiClass;
	}

	public List<Example> getExamples() {
		return this.examples;
	}

	public void setExamples(List<Example> examples) {
		this.examples = examples;
	}

}