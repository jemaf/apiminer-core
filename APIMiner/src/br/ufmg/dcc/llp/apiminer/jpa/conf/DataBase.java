/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.jpa.conf;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author edumontandon
 *
 */
public class DataBase {
	
	private static final String PERSISTENCE_UNIT = "apiminer-core";
	private static EntityManagerFactory factory;
		
	public static EntityManagerFactory getFactory() {
		
		if(factory == null)
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		return factory;
	}
	

}
