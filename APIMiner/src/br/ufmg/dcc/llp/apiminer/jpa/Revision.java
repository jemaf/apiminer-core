package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the revision database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Revision")
public class Revision implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String author;

	private String datecommit;

	private String message;

	private Long revisionorder;

	private Timestamp timeins;

	//bi-directional many-to-one association to Modification
	@OneToMany(mappedBy="revision")
	private List<Modification> modifications;

	//bi-directional many-to-one association to Project
	@ManyToOne
	private Project project;

	public Revision() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDatecommit() {
		return this.datecommit;
	}

	public void setDatecommit(String datecommit) {
		this.datecommit = datecommit;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getRevisionorder() {
		return this.revisionorder;
	}

	public void setRevisionorder(Long revisionorder) {
		this.revisionorder = revisionorder;
	}

	public Timestamp getTimeins() {
		return this.timeins;
	}

	public void setTimeins(Timestamp timeins) {
		this.timeins = timeins;
	}

	public List<Modification> getModifications() {
		return this.modifications;
	}

	public void setModifications(List<Modification> modifications) {
		this.modifications = modifications;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}