package br.ufmg.dcc.llp.apiminer.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the artifact database table.
 * 
 */
@Entity(name="br.ufmg.dcc.llp.apiminer.jpa.Artifact")
public class Artifact implements Serializable, IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String filepath;

	private Timestamp timeins;

	//bi-directional many-to-one association to Example
	@OneToMany(mappedBy="artifact")
	private List<Example> examples;

	//bi-directional many-to-one association to Modification
	@OneToMany(mappedBy="artifact")
	private List<Modification> modifications;

	public Artifact() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFilepath() {
		return this.filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Timestamp getTimeins() {
		return this.timeins;
	}

	public void setTimeins(Timestamp timeins) {
		this.timeins = timeins;
	}

	public List<Example> getExamples() {
		return this.examples;
	}

	public void setExamples(List<Example> examples) {
		this.examples = examples;
	}

	public List<Modification> getModifications() {
		return this.modifications;
	}

	public void setModifications(List<Modification> modifications) {
		this.modifications = modifications;
	}

}