/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.test;

import org.eclipse.jdt.core.JavaModelException;

import br.ufmg.dcc.llp.apiminer.annotation.parser.JDTParser;
import br.ufmg.dcc.llp.apiminer.jpa.Example;

/**
 * @author edumontandon
 *
 */
public class AnnotationTest {
	public static void main(String args[]) throws JavaModelException {

		JDTParser parser = new JDTParser(args);
		parser.parse();
		
		for (Example ex : parser.getExamples()) {
	        System.out.println(ex);
	        System.out.println("=================================================");
        }
	}
}
