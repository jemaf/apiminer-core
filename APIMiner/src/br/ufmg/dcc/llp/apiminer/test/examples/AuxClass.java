/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.test.examples;

/**
 * @author edumontandon
 */
public class AuxClass {
	public int theNumber;
	private int myNewVar;

	/**
	 * @param m
	 */
	public AuxClass(int m) {
		theNumber = m;
	}

	public AuxClass() {
		theNumber = 1;
	}

	int f(int x) {
		return x + 3;
	}

	/**
	 * @return
	 */
	public int getRandomNumber() {
		int[] a = new int[13];
		int[] d = new int[10];
		d[3] = 5;
		int h = d[0];
		d[h] = 25;
		System.out.println(f(f(d[0] + a[0])));
		return (int) (theNumber * Math.random() * getInteger(myNewVar) * 1000);
	}

	/**
	 * @param theNumber2
	 * @return
	 */
	private int getInteger(int theNumber2) {
		return theNumber2;
	}

	/**
	 * @param class2
	 */
	public void add(AuxClass class2) {
		this.theNumber += class2.myNewVar;
	}

	/**
	 * @param class1
	 */
	public void sub(AuxClass class1) {
		this.theNumber -= class1.theNumber;
	}
}
