package br.ufmg.dcc.llp.apiminer.test.examples;

public interface Imp {

	int varint1 = 2;
	
	public void impM1();
	public void impM2();
	
}
