package br.ufmg.dcc.llp.apiminer.test;

import br.ufmg.dcc.llp.apiminer.build.android.AndroidBuild;
import br.ufmg.dcc.llp.apiminer.dao.ProjectDAO;
import br.ufmg.dcc.llp.apiminer.dao.RepositoryTypeDAO;
import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.jpa.Repository;
import br.ufmg.dcc.llp.apiminer.jpa.Repositorytype;
import br.ufmg.dcc.llp.apiminer.main.PreProcessing;

public class PreProcessingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

//		ProjectDAO pdao = new ProjectDAO();
//		List<Project> projects = pdao.getAll();
//
//		for (Project project : projects) {
//			PreProcessing.updateProject(project, new AndroidBuild());
//		}

//		Project p = new Project();
//		Repository r = new Repository();
//		Repositorytype repositorytype = new Repositorytype();
//		
//		repositorytype.setName("Subversion");
//		RepositoryTypeDAO typeDAO = new RepositoryTypeDAO();
//		repositorytype= typeDAO.select(repositorytype).get(0);
//		
//		r.setFolderName("corporate-address-book");
//		r.setUrladdress("http://corporateaddressbook.googlecode.com/svn/trunk/");
//		r.setRepositorytype(repositorytype);
//		
//		p.setName("Corporate Address Book");
//		p.setRepository(r);
//				
//		PreProcessing.processProject(p, new AndroidBuild());
		
		 long id = 12332;
		 ProjectDAO pdao = new ProjectDAO();
		 Project project = new Project();
		 project.setId(id);
		 project = pdao.select(project).get(0);
		 PreProcessing.updateProject(project, new AndroidBuild());
	}
	
}
