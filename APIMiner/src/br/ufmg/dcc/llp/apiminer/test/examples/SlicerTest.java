package br.ufmg.dcc.llp.apiminer.test.examples;

import java.io.File;

public class SlicerTest {
	/**
	 * @param i
	 * @return
	 */
	private Integer getInteger(int i) {
		return i;
	}

	public void sliceTest1() {
		Integer a = getInteger(2);
		double b = Math.random();
		double c = Math.random();
		if (c < b) {
			System.out.println("first print");
			Double d = a.doubleValue();
			a = (int) d.doubleValue();
			System.out.println("second print");
		} else 
			a = (int) new Integer(30).doubleValue();
		
		System.out.println("third print");
		Double d2 = (double) a.intValue();
	}

	public void sliceTest2() {
		Integer a = getInteger(2); //
		Double d = 3.14;
		if (1 < d) { //
			System.out.println("first print");
			d = a.doubleValue(); //
			System.out.println("second print");
		} //
		Double d2 = (double) a.intValue(); //
	}

	public void sliceTest3() {
		Integer a = getInteger(2); //
		Double d = 3.14;
		if (1 < a) { //
			System.out.println("first print");
			System.out.println("second print");
			d = a.doubleValue(); //
			System.out.println("third print");
		} //
		Double d2 = (double) a.intValue(); //
	}

	public void sliceTest4() {
		Integer a = null;
		Integer b = 5;
		while (b < 20) {
			a = getInteger(2);
			System.out.println("first print");
			System.out.println("second print");
		}
		Double d2 = (double) a.intValue();
	}

	public void sliceWikipedia() {
		int i;
		int sum = new Integer(0).intValue();
		int product = new Integer(1);
		int power = 1;
		int N = new Integer(10);
		for (i = 0; i < N; ++i) {
			sum = sum + i;
			product = product * i;
			power = power * power;
		}
		write(sum);
		write(product);
		write(power);
	}

	public void sliceWikipedia2() {
		int i = Integer.parseInt("123");
		int sum = Integer.parseInt("321");
		int product = Integer.parseInt("1");
		int N = (int) (Math.random() * 100);
		for (i = 0; i < N; ++i) {
			sum = sum + i;
			product = product * i;
		}
		write(sum);
		write(product);
	}

	
	public void tryTest() {
		
		int a = getInteger(2); //
		
		try { //
			File f = new File(""+a); //
		} catch(Exception ex) { //
			System.out.println("oi");
			System.err.println("mundo");
			int b = a; //
			a = 25;
		} //
		finally  { //
			System.out.println("first");
			System.out.println(a); //
			System.out.println("second");
		} //
		
		System.out.println("Final a: ");
		System.out.println(a); //
		
	}
	
	/**
	 * @param sum
	 */
	private void write(int sum) {
	}
}