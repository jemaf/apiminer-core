package br.ufmg.dcc.llp.apiminer.annotation.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FileASTRequestor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.jpa.ApiMethod;
import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.jpa.Project;
import br.ufmg.dcc.llp.apiminer.util.Util;

/**
 * @author edumontandon
 * 
 */
public class JDTParser {

	private static final Logger parserLogging = LoggerFactory.getLogger(JDTParser.class);
	
	/**
	 * default compiling version
	 */
	public static final String DEFAULT_CODE_VERSION = JavaCore.VERSION_1_5;

	/**
	 * list of classpath
	 */
	private List<String> cp;

	/**
	 * list of sourcepath
	 */
	private List<String> sp;

	/**
	 * list of files that will be analyzed
	 */
	private ArrayList<String> files;

	private String seed = null;

	/**
	 * Java parser, provided by JDT
	 */
	private ASTParser parser = null;

	/**
	 * List of collected examples
	 */
	private ArrayList<Example> examples;

	/**
	 * @param args
	 */
	public JDTParser(String[] args) {

		this.parser = ASTParser.newParser(AST.JLS3);
		this.cp = new ArrayList<String>();
		this.sp = new ArrayList<String>();
		this.files = new ArrayList<String>();
		this.examples = new ArrayList<Example>();

		handleEntries(args);
		setOptions();

	}

	/**
	 * @param path
	 * @param cp
	 * @param clazz
	 * @param method
	 * @param sys
	 */
	public JDTParser(String path, String cp, ApiMethod method, Project p) {

		this.parser = ASTParser.newParser(AST.JLS3);
		this.cp = new ArrayList<String>();
		this.sp = new ArrayList<String>();
		this.files = new ArrayList<String>();
		this.examples = new ArrayList<Example>();

		String fullSystemPath = path + File.separator + p.getRepository().getFolderName();
		String fullSeed = method.getApiClass().getName() + "." + method.getFqn();

		String args[] = new String[] { "-cp", cp, "-m", fullSeed, "-sp", fullSystemPath, "-files",
		        fullSystemPath };

		handleEntries(args);
	}

	/**
	 * Handle the parameters of the program
	 * 
	 * @param args
	 *            list of arguments
	 */
	private void handleEntries(String[] args) {

		Map<String, ArrayList<String>> map = Util.handleEntries(args);
		this.cp = map.get("-cp");
		this.seed = map.get("-m").get(0);
		ArrayList<String> listTempFiles = map.get("-files");
		this.sp = map.get("-sp");

		// get the files that will be used in the parsing process
		for (String fileOrDir : listTempFiles) {
			File f = new File(fileOrDir);
			if (f.exists())
				if (f.isDirectory())
					files.addAll(Util.collectFiles(fileOrDir, ".java"));
				else
					files.add(fileOrDir);
		}

	}

	/**
	 * set the parser options
	 */
	private void setOptions() {

		parser.setEnvironment(cp.toArray(new String[cp.size()]), sp.toArray(new String[sp.size()]),
		        null, true);
		parser.setResolveBindings(true);
		parser.setBindingsRecovery(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		@SuppressWarnings("unchecked")
		Map<String, String> options = JavaCore.getOptions();
		options.put(JavaCore.COMPILER_COMPLIANCE, DEFAULT_CODE_VERSION);
		options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, DEFAULT_CODE_VERSION);
		options.put(JavaCore.COMPILER_SOURCE, DEFAULT_CODE_VERSION);

		parser.setCompilerOptions(options);
	}

	/**
	 * execute the parser process
	 */
	public void parse() {
		setOptions();
		FileASTRequestor requestor = new FileASTRequestor() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.eclipse.jdt.core.dom.FileASTRequestor#acceptAST(java.lang
			 * .String, org.eclipse.jdt.core.dom.CompilationUnit)
			 */
			@Override
			public void acceptAST(String sourceFilePath, CompilationUnit ast) {
				parserLogging.debug("Executing file " + sourceFilePath);
				AnnotationVisitor visitor = new AnnotationVisitor(seed, sourceFilePath, ast);
				ast.accept(visitor);
				examples.addAll(visitor.getExamples());
			}
		};
		parser.createASTs((files.toArray(new String[files.size()])), null, new String[0],
		        requestor, null);
	}

	public ArrayList<Example> getExamples() {
		return this.examples;
	}
}
