/**
 * 
 */
package br.ufmg.dcc.llp.apiminer.annotation.parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.compress.utils.Charsets;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jdt.core.formatter.DefaultCodeFormatterConstants;
import org.eclipse.jdt.internal.corext.dom.ASTNodes;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufmg.dcc.llp.apiminer.jpa.Example;
import br.ufmg.dcc.llp.apiminer.util.Util;

/**
 * @author edumontandon
 * 
 */
public class AnnotationVisitor extends ASTVisitor {

	private static final Logger exampleLogging = LoggerFactory
			.getLogger(Example.class);

	private static final int MAX_LOOP = br.ufmg.dcc.llp.apiminer.util.Util
			.getMaxLoopValue();

	private String seedMethodName;

	private String fileName;

	private StringBuffer completeFile;

	private ArrayList<Example> examples;

	private CompilationUnit cu;

	private String seedTypeName;

	private ArrayList<String> arguments;

	enum ID_TYPE {
		DEFINITION, DECLARATION, READ
	}

	public AnnotationVisitor(String seed, String sourceFilePath,
			CompilationUnit ast) {
		this.cu = ast;

		File f = new File(sourceFilePath);
		completeFile = new StringBuffer();
		List<String> list;

		try {
			list = Files.readAllLines(f.toPath(), Charsets.UTF_8);
			for (String line : list) {
				completeFile.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		int indexSpearator = sourceFilePath
				.lastIndexOf((File.separator + "src" + File.separator));

		this.fileName = indexSpearator != -1 ? sourceFilePath.substring(
				indexSpearator, sourceFilePath.length()) : sourceFilePath
				.replace(Util.getProjectsSourcepath(), "");
		this.examples = new ArrayList<Example>();
		this.arguments = new ArrayList<String>();

		handleSeed(seed);

	}

	/**
	 * Handle the seed method passes as parameter.
	 * 
	 * @param seed
	 *            the seed method that will be handled
	 */
	private void handleSeed(String seed) {
		seed = seed.trim();

		int beginIndex = seed.indexOf("(");
		int endIndex = seed.indexOf(")");
		String parameters = seed.substring(beginIndex + 1, endIndex);
		if (parameters.length() > 0)
			for (String argument : Arrays.asList(parameters.split(","))) {
				arguments.add(argument.trim());
			}

		String typeAndName = seed.split("\\(")[0];
		this.seedMethodName = typeAndName.substring(
				typeAndName.lastIndexOf(".") + 1, typeAndName.length());
		this.seedTypeName = typeAndName.substring(0,
				typeAndName.lastIndexOf("."));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * MethodInvocation)
	 */
	@Override
	public boolean visit(MethodInvocation node) {

		if (ASTNodes.getParent(node, ASTNode.METHOD_DECLARATION) != null
				&& this.isSeedMethod(node)) {
			extractExamples(node);
		}
		return super.visit(node);
	}

	/**
	 * Check if the invocation method is a seed method
	 * 
	 * @param node
	 *            the node that will be verified
	 * @return true if it is, false otherwise
	 */
	private boolean isSeedMethod(MethodInvocation node) {
		IMethodBinding method = node.resolveMethodBinding();
		if (method != null) {
			ITypeBinding type = method.getDeclaringClass();

			// FIXME: discover why type has got null values
			// FIXME: NP when analyzes anonnymous class
			if (type != null
					&& (type.isAnonymous() || !type.getBinaryName().equals(
							this.seedTypeName))
					|| !node.getName().toString().equals(this.seedMethodName))
				return false;

			ITypeBinding[] params = method.getParameterTypes();
			boolean hasChecked = true;

			if (params.length == arguments.size())
				for (int i = 0; i < params.length && hasChecked; i++) {

					if (!params[i].getQualifiedName().equals(arguments.get(i)))
						hasChecked = false;
				}
			else
				hasChecked = false;

			if (!hasChecked)
				return false;
			else
				return true;
		} else
			return false;
	}

	/**
	 * @param node
	 */
	private void extractExamples(MethodInvocation node) {
		// get the seed command
		ASTNode command = node;
		while (!this.isCompositeNode(command.getParent()))
			command = command.getParent();
		String seed = command.toString();

		// get the whole method declaration
		MethodDeclaration declaration = (MethodDeclaration) ASTNodes.getParent(
				node, ASTNode.METHOD_DECLARATION);
		String sourceMethod = declaration.getName().toString()
				+ declaration.parameters().toString().replace('[', '(')
						.replace(']', ')');

		try {
			// mark the nodes that are important to the example
			Collection<ASTNode> lines = this.markNodes(node,
					Arrays.asList(ID_TYPE.values()), MAX_LOOP);
			// remove the unnecessary nodes
			String codeFragment = this.removeUnnecessaryNodes(lines);

			// create the example and insert it into the database
			Example ex = new Example();
			ex.setFile(this.fileName);
			ex.setSeed(seed);
			ex.setExample(codeFragment);
			ex.setSourceMethod(sourceMethod);
			ex.setNumFileCheckout(0);
			ex.setSourceFile(this.completeFile.toString());

			this.examples.add(ex);

		} catch (MalformedTreeException e) {
			String errorLog = "seed: " + seed.replace("\n", "") + "\nfile: "
					+ this.fileName + "\nsource method:\n" + sourceMethod;
			exampleLogging.error("Could not process the example\n" + errorLog,
					e);
		} catch (BadLocationException e) {
			String errorLog = "seed: " + seed.replace("\n", "") + "\nfile: "
					+ this.fileName + "\nsource method:\n" + sourceMethod;
			exampleLogging.error("Could not process the example\n" + errorLog,
					e);
		} catch (Exception e) {
			String errorLog = "seed: " + seed.replace("\n", "") + "\nfile: "
					+ this.fileName + "\nsource method:\n" + sourceMethod;
			exampleLogging.error(
					"Could not process the example: UNKOWN ERROR\n" + errorLog,
					e);
		}
	}

	/**
	 * Remove the nodes that are unnecessary to the example
	 * 
	 * @param nodes
	 * 
	 * @return a String that represents the example
	 * @throws BadLocationException
	 * @throws MalformedTreeException
	 */
	@SuppressWarnings("unchecked")
	private String removeUnnecessaryNodes(Collection<ASTNode> nodes)
			throws MalformedTreeException, BadLocationException {
		// for the removal of the unnecessary statements, we based on
		// ASTRewrite API. Basically, we re-parse the fragment where the
		// example is presented and, from this, we remove the nodes that
		// represent the unnecessary statements.

		if (nodes.size() > 0) {

			// get the methodDeclaration node of the example
			MethodDeclaration methodNode = (MethodDeclaration) ASTNodes
					.getParent((ASTNode) nodes.toArray()[0],
							ASTNode.METHOD_DECLARATION);

			// get the fragment where the example is present and parse it
			String code = "";
			for (Object astNode : methodNode.getBody().statements())
				code = code + astNode.toString();
			ASTParser parser = ASTParser.newParser(AST.JLS3);
			parser.setKind(ASTParser.K_STATEMENTS);
			parser.setSource(code.toCharArray());
			ASTNode parentNode = parser.createAST(null);

			// get the AST representation of the unnecessary nodes (it is really
			// necessary)
			Stack<ASTNode> stack = new Stack<ASTNode>();
			HashSet<ASTNode> reservedStatements = new HashSet<ASTNode>();
			ArrayList<ASTNode> unnecessaryStms = new ArrayList<ASTNode>();
			stack.addAll(((Block) parentNode).statements());
			while (!stack.isEmpty()) {
				ASTNode newNode = stack.pop();
				boolean shouldRemove = true;

				for (ASTNode originalNode : nodes)
					if (originalNode.toString().equals(newNode.toString())) {
						shouldRemove = false;
						// add the switch-case and break statements as reserved
						// statements
						if (ASTNodes.getParent(originalNode,
								ASTNode.SWITCH_STATEMENT) != null)
							reservedStatements.addAll(this
									.handleSwitchCase(newNode));
					}

				if (shouldRemove && newNode.getNodeType() != ASTNode.BLOCK)
					unnecessaryStms.add(newNode);

				if (this.isCompositeNode(newNode)) {
					stack.addAll(this.getSubStatements(newNode));
				}
			}

			// remove the reserved statements that were considered unnecessary
			// (switch-case and break statements)
			for (ASTNode unnNode : (Collection<ASTNode>) unnecessaryStms
					.clone())
				if (reservedStatements.contains(unnNode))
					unnecessaryStms.remove(unnNode);

			// remove the unnecessary nodes
			String strCode = removeStatements(code, parentNode, unnecessaryStms);
			// reformat the code snippet
			strCode = reformatCode(strCode);
			return strCode;
		}
		return null;
	}

	/**
	 * @param code
	 * @return
	 * @throws BadLocationException
	 * @throws MalformedTreeException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String reformatCode(String source) throws MalformedTreeException,
			BadLocationException {

		// take default Eclipse formatting options
		Map options = DefaultCodeFormatterConstants.getEclipseDefaultSettings();

		// initialize the compiler settings to be able to format 1.5 code
		options.put(JavaCore.COMPILER_COMPLIANCE, JavaCore.VERSION_1_5);
		options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM,
				JavaCore.VERSION_1_5);
		options.put(JavaCore.COMPILER_SOURCE, JavaCore.VERSION_1_5);

		// change the option to wrap each enum constant on a new line
		options.put(
				DefaultCodeFormatterConstants.FORMATTER_ALIGNMENT_FOR_ENUM_CONSTANTS,
				DefaultCodeFormatterConstants.createAlignmentValue(true,
						DefaultCodeFormatterConstants.WRAP_ONE_PER_LINE,
						DefaultCodeFormatterConstants.INDENT_ON_COLUMN));

		// instantiate the default code formatter with the given options
		CodeFormatter codeFormatter = ToolFactory.createCodeFormatter(options);

		TextEdit edit = codeFormatter.format(CodeFormatter.K_STATEMENTS,
				source, 0, source.length(), 0,
				System.getProperty("line.separator"));

		IDocument document = new Document(source);
		edit.apply(document);

		return document.get();
	}

	private String removeStatements(String code, ASTNode parentNode,
			ArrayList<ASTNode> unnecessaryStms) throws MalformedTreeException,
			BadLocationException {

		Document document = new Document(code);

		// remove the unnecessary statements
		ASTRewrite rw = ASTRewrite.create(parentNode.getAST());
		for (ASTNode astNode : unnecessaryStms) {

			if (this.isCompositeNode(astNode.getParent())
					&& astNode.getParent().getNodeType() != ASTNode.BLOCK
					&& astNode.getParent().getNodeType() != ASTNode.SWITCH_STATEMENT) {
				Block emptyBlock = astNode.getParent().getAST().newBlock();
				rw.replace(astNode, emptyBlock, null);
			} else {
				rw.remove(astNode, null);
			}
		}

		// search for empty composite nodes to be removed
		ArrayList<ASTNode> compositeNodes = this
				.getAllCompositeNodes(parentNode);
		for (ASTNode compositeNode : compositeNodes) {
			boolean isEmpty = true;
			Collection<ASTNode> childrenNodes = this
					.getSubStatements(compositeNode);
			for (ASTNode child : childrenNodes) {
				if (!unnecessaryStms.contains(child))
					isEmpty = false;
			}

			if (isEmpty && compositeNode.getNodeType() != ASTNode.CATCH_CLAUSE)
				rw.remove(compositeNode, null);
		}

		// apply the changes
		TextEdit text = rw.rewriteAST(document, null);
		text.apply(document);
		return document.get();
	}

	/**
	 * Get all the children nodes that are composite nodes
	 * 
	 * @param root
	 *            the parent node
	 * @return a list with composite nodes
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<ASTNode> getAllCompositeNodes(ASTNode root) {

		Stack<ASTNode> stack = new Stack<ASTNode>();
		ArrayList<ASTNode> result = new ArrayList<ASTNode>();
		stack.add(root);

		while (!stack.isEmpty()) {

			ASTNode astNode = stack.pop();

			if (isCompositeNode(astNode)) {
				if ((astNode.getNodeType() == ASTNode.BLOCK && (astNode
						.getParent().getNodeType() == ASTNode.BLOCK || astNode
						.getParent().getNodeType() == ASTNode.SWITCH_STATEMENT))
						|| astNode.getNodeType() != ASTNode.BLOCK)
					result.add(astNode);
				stack.addAll(ASTNodes.getChildren(astNode));
			}
		}
		return result;
	}

	/**
	 * @param astNode
	 * @return
	 */
	private boolean isCompositeNodeEmpty(ASTNode astNode,
			ArrayList<ASTNode> includedNodes) {

		Collection<ASTNode> subNodes = this.getSubStatements(astNode);

		for (ASTNode subNode : subNodes) {
			if (includedNodes.contains(subNode))
				return false;
		}

		return true;
	}

	/**
	 * Mark the lines that do not have a correlation with the node passed as
	 * parameter
	 * 
	 * @param nodeSeed
	 *            seed of the annotation algorithm
	 * @return a map of line numbers per strings that represents the line itself
	 */
	@SuppressWarnings("unchecked")
	private Collection<ASTNode> markNodes(ASTNode nodeSeed,
			List<ID_TYPE> typesToConsider, int loopLevel) {
		HashSet<ASTNode> result = new HashSet<ASTNode>();

		// get the MethodDeclaration parent node
		ASTNode methodNode = nodeSeed.getParent();
		if (methodNode != null) {
			while (methodNode.getNodeType() != ASTNode.METHOD_DECLARATION) {

				// get the most top node that represents the statement that
				// invokes
				// the method
				if (!this.isCompositeNode(methodNode)
						&& !this.isCompositeNode(nodeSeed))
					nodeSeed = methodNode;
				methodNode = methodNode.getParent();
			}

			Map<ID_TYPE, ArrayList<ASTNode>> myMap = new HashMap<AnnotationVisitor.ID_TYPE, ArrayList<ASTNode>>();
			myMap.put(ID_TYPE.READ, new ArrayList<ASTNode>());
			myMap.put(ID_TYPE.DECLARATION, new ArrayList<ASTNode>());
			myMap.put(ID_TYPE.DEFINITION, new ArrayList<ASTNode>());

			// get the read and write nodes of the statement
			this.getReadDeclDefNodes(nodeSeed, myMap);
			ArrayList<ASTNode> reads = typesToConsider.contains(ID_TYPE.READ) ? myMap
					.get(ID_TYPE.READ) : new ArrayList<ASTNode>();
			ArrayList<ASTNode> decls = typesToConsider
					.contains(ID_TYPE.DECLARATION) ? myMap
					.get(ID_TYPE.DECLARATION) : new ArrayList<ASTNode>();
			ArrayList<ASTNode> defs = typesToConsider
					.contains(ID_TYPE.DEFINITION) ? myMap
					.get(ID_TYPE.DEFINITION) : new ArrayList<ASTNode>();

			// get the ids of composite nodes that are parent of the seed, if
			// they exist
			result.add(nodeSeed);
			while (nodeSeed.getParent() != ((MethodDeclaration) methodNode)
					.getBody()) {
				nodeSeed = nodeSeed.getParent();
				if (this.isCompositeNode(nodeSeed)
						&& nodeSeed.getNodeType() != ASTNode.BLOCK) {
					result.add(nodeSeed);
					this.getReadDeclDefNodes(nodeSeed, myMap);
				}
			}

			// annotate unnecessary lines according to read nodes
			for (ASTNode readNode : reads) {
				result.addAll(markNodes(
						readNode,
						((MethodDeclaration) methodNode).getBody().statements(),
						ID_TYPE.DECLARATION, loopLevel));
				result.addAll(markNodes(
						readNode,
						((MethodDeclaration) methodNode).getBody().statements(),
						ID_TYPE.DEFINITION, loopLevel));
			}

			// annotate unnecessary lines according to declaration nodes
			for (ASTNode declNode : decls) {
				result.addAll(markNodes(
						declNode,
						((MethodDeclaration) methodNode).getBody().statements(),
						ID_TYPE.READ, loopLevel));
			}

			// annotate unnecessary lines according to definition nodes
			for (ASTNode defNode : defs) {
				result.addAll(markNodes(
						defNode,
						((MethodDeclaration) methodNode).getBody().statements(),
						ID_TYPE.DECLARATION, loopLevel));
				result.addAll(markNodes(
						defNode,
						((MethodDeclaration) methodNode).getBody().statements(),
						ID_TYPE.READ, loopLevel));
			}
		}
		return result;
	}

	/**
	 * Execute the annotation algorithm according to the flag passed as
	 * parameter
	 * 
	 * @param id
	 * @param body
	 * @param type
	 * @param loopLevel
	 * @return
	 */
	private ArrayList<ASTNode> markNodes(ASTNode id, Collection<ASTNode> body,
			ID_TYPE type, int loopLevel) {

		ArrayList<ASTNode> lines = new ArrayList<ASTNode>();
		ArrayList<ASTNode> ids = new ArrayList<ASTNode>();

		for (Object node : body) {
			ASTNode stm = (ASTNode) node;

			if (isOk(id, type, stm)) {

				ids = this.getReadDeclDefNodes(stm,
						new HashMap<ID_TYPE, ArrayList<ASTNode>>()).get(type);

				// if is a simple statement, proceed normally to mark it.
				// Otherwise, verify if it is necessary to mark any
				// expression of composite statement
				if (!this.isCompositeNode(stm)) {

					if (this.idIsPresent(ids, id)) {
						lines.add(stm);
						if (loopLevel > 0) {
							lines.addAll(this.markNodes(stm, Arrays
									.asList(new ID_TYPE[] { ID_TYPE.READ }),
									--loopLevel));
						}
					}
				} else {
					// get the sub statements of this node, if any
					Collection<ASTNode> subStatements = this
							.getSubStatements(stm);

					ArrayList<ASTNode> temp = new ArrayList<ASTNode>();
					temp.addAll(this.markNodes(id, subStatements, type,
							loopLevel));

					// if the unnecessary nodes' list has the same size of all
					// sub-statements and the parent statement does not contain
					// the id, mark all the block
					if (!this.isCompositeNodeEmpty(stm, temp)
							&& (temp.size() > 0 || this.idIsPresent(ids, id))) {
						temp.add(stm);
						if (loopLevel > 0) {
							lines.addAll(this.markNodes(stm, Arrays
									.asList(new ID_TYPE[] { ID_TYPE.READ }),
									--loopLevel));
						}
					}

					lines.addAll(temp);
				}
			}
		}
		return lines;
	}

	/**
	 * @param id
	 * @param type
	 * @param stm
	 * @return
	 */
	private boolean isOk(ASTNode id, ID_TYPE type, ASTNode stm) {

		int firstLineStm = this.cu.getLineNumber(stm.getStartPosition());
		int lastLineStm = this.cu.getLineNumber(stm.getStartPosition()
				+ stm.getLength());
		int lineId = this.cu.getLineNumber(id.getStartPosition());

		if (firstLineStm == lastLineStm)
			return (firstLineStm < lineId && (type == ID_TYPE.DECLARATION || type == ID_TYPE.DEFINITION))
					|| (firstLineStm > lineId && type == ID_TYPE.READ);
		else
			return ((firstLineStm < lineId || lineId < lastLineStm) && (type == ID_TYPE.DECLARATION || type == ID_TYPE.DEFINITION))
					|| ((firstLineStm > lineId || lineId < lastLineStm) && type == ID_TYPE.READ);
	}

	/**
	 * verify if the node is a composite node (a node that contains statements)
	 * 
	 * @param astNode
	 * @return
	 */
	private boolean isCompositeNode(ASTNode astNode) {

		return !(this.getSubStatements(astNode).size() <= 1 && this
				.getSubStatements(astNode).contains(astNode));
	}

	/**
	 * get a list of child nodes based on parent node
	 * 
	 * @param stm
	 *            the parent node
	 * @return a list of parent node's children, if any, or the own parent node
	 */
	private Collection<ASTNode> getSubStatements(ASTNode root) {

		List<ASTNode> result = new ArrayList<ASTNode>();

		if (root != null)
			switch (root.getNodeType()) {
			case ASTNode.BLOCK:
				for (Object astNode : ((Block) root).statements())
					result.addAll(this.getSubStatements((ASTNode) astNode));
				break;
			case ASTNode.CATCH_CLAUSE:
				result.addAll(this.getSubStatements(((CatchClause) root)
						.getBody()));
				break;
			case ASTNode.DO_STATEMENT:
				result.addAll(this.getSubStatements(((DoStatement) root)
						.getBody()));
				break;
			case ASTNode.FOR_STATEMENT:
				result.addAll(this.getSubStatements(((ForStatement) root)
						.getBody()));
				break;
			case ASTNode.IF_STATEMENT:
				result.addAll(this.getSubStatements(((IfStatement) root)
						.getThenStatement()));
				result.addAll(this.getSubStatements(((IfStatement) root)
						.getElseStatement()));
				break;
			case ASTNode.SYNCHRONIZED_STATEMENT:
				result.addAll(this
						.getSubStatements(((SynchronizedStatement) root)
								.getBody()));
				break;
			case ASTNode.SWITCH_STATEMENT:
				for (Object astNode : ((SwitchStatement) root).statements())
					result.addAll(this.getSubStatements((ASTNode) astNode));
				break;
			case ASTNode.TRY_STATEMENT:
				result.addAll(this.getSubStatements(((TryStatement) root)
						.getBody()));
				result.addAll(this.getSubStatements(((TryStatement) root)
						.getFinally()));
				for (Object astNode : ((TryStatement) root).catchClauses()) {
					result.addAll(this.getSubStatements((ASTNode) astNode));
				}
				break;
			case ASTNode.WHILE_STATEMENT:
				result.addAll(this.getSubStatements(((WhileStatement) root)
						.getBody()));
				break;
			default:
				result.add(root);
			}

		return result;
	}

	/**
	 * Verifiy if an id is present in a list of ids
	 * 
	 * @param ids
	 *            list of ids
	 * @param id
	 *            the id that will be checked
	 * @return true if the id is present, false otherwise
	 */
	private boolean idIsPresent(ArrayList<ASTNode> ids, ASTNode id) {

		if (ids != null)
			for (ASTNode astNode : ids) {
				SimpleName name = (SimpleName) astNode;
				if (name.getIdentifier().equals(
						((SimpleName) id).getIdentifier()))
					return true;
			}
		return false;
	}

	/**
	 * Get a map of the statement's node ids classified by their type (READ,
	 * DECLARATION or DEFINITION)
	 * 
	 * @param root
	 *            the node that represents the statement that contains the node
	 *            ids
	 * @param result
	 *            an empty Map
	 * @return a map of node ids per type
	 */
	private Map<ID_TYPE, ArrayList<ASTNode>> getReadDeclDefNodes(ASTNode root,
			Map<ID_TYPE, ArrayList<ASTNode>> result) {

		ArrayList<ASTNode> read = result.get(ID_TYPE.READ) != null ? result
				.get(ID_TYPE.READ) : new ArrayList<ASTNode>();
		ArrayList<ASTNode> decl = result.get(ID_TYPE.DECLARATION) != null ? result
				.get(ID_TYPE.DECLARATION) : new ArrayList<ASTNode>();
		ArrayList<ASTNode> def = result.get(ID_TYPE.DEFINITION) != null ? result
				.get(ID_TYPE.DEFINITION) : new ArrayList<ASTNode>();

		if (root != null)
			switch (root.getNodeType()) {

			// AnonymousClassDeclaration:
			// { ClassBodyDeclaration }
			case ASTNode.ANONYMOUS_CLASS_DECLARATION:
				AnonymousClassDeclaration cl = (AnonymousClassDeclaration) root;
				for (Object astNode : cl.bodyDeclarations()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// Expression [ Expression ]
			case ASTNode.ARRAY_ACCESS:
				ArrayAccess arrayAccStm = (ArrayAccess) root;
				result.putAll(this.getReadDeclDefNodes(arrayAccStm.getArray(),
						result));
				result.putAll(this.getReadDeclDefNodes(arrayAccStm.getIndex(),
						result));
				break;

			// Expression AssignmentOperator Expression
			case ASTNode.ASSIGNMENT:
				Assignment assStm = (Assignment) root;
				result.putAll(this.getReadDeclDefNodes(
						assStm.getLeftHandSide(), result));
				result.putAll(this.getReadDeclDefNodes(
						assStm.getRightHandSide(), result));
				break;

			// { { Statement } }
			case ASTNode.BLOCK:
				Block blockStm = (Block) root;
				for (Object astNode : blockStm.statements()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// break [ Identifier ] ;
			case ASTNode.BREAK_STATEMENT:
				BreakStatement breakStm = (BreakStatement) root;
				result.putAll(this.getReadDeclDefNodes(breakStm.getLabel(),
						result));
				break;

			// ( Type ) Expression
			case ASTNode.CAST_EXPRESSION:
				CastExpression castStm = (CastExpression) root;
				result.putAll(this.getReadDeclDefNodes(castStm.getExpression(),
						result));
				break;

			// catch ( FormalParameter ) Block
			case ASTNode.CATCH_CLAUSE:
				CatchClause catchStm = (CatchClause) root;
				// result.putAll(this.getReadDeclDefNodes(catchStm.getBody(),
				// result));
				result.putAll(this.getReadDeclDefNodes(catchStm.getException(),
						result));
				break;

			// [ Expression . ]
			// new [ < Type { , Type } > ]
			// Type ( [ Expression { , Expression } ] )
			// [ AnonymousClassDeclaration ]
			case ASTNode.CLASS_INSTANCE_CREATION:
				ClassInstanceCreation classInstStm = (ClassInstanceCreation) root;
				result.putAll(this.getReadDeclDefNodes(
						classInstStm.getExpression(), result));
				for (Object astNode : classInstStm.arguments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}

				if (classInstStm.getAnonymousClassDeclaration() != null)
					result.putAll(this.getReadDeclDefNodes(
							classInstStm.getAnonymousClassDeclaration(), result));
				break;

			// Expression ? Expression : Expression
			case ASTNode.CONDITIONAL_EXPRESSION:
				ConditionalExpression condExpStm = (ConditionalExpression) root;
				result.putAll(this.getReadDeclDefNodes(
						condExpStm.getThenExpression(), result));
				result.putAll(this.getReadDeclDefNodes(
						condExpStm.getElseExpression(), result));
				result.putAll(this.getReadDeclDefNodes(
						condExpStm.getExpression(), result));
				break;

			// [ < Type { , Type } > ]
			// this ( [ Expression { , Expression } ] ) ;
			case ASTNode.CONSTRUCTOR_INVOCATION:
				ConstructorInvocation constInv = (ConstructorInvocation) root;
				for (Object node : constInv.arguments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) node,
							result));
				}
				break;

			// continue [ Identifier ] ;
			case ASTNode.CONTINUE_STATEMENT:
				// System.err.println("Not implemented yet" + root);
				break;

			// do Statement while ( Expression ) ;
			case ASTNode.DO_STATEMENT:
				DoStatement doStm = (DoStatement) root;
				result.putAll(this.getReadDeclDefNodes(doStm.getExpression(),
						result));
				// result.putAll(this.getReadDeclDefNodes(doStm.getBody(),
				// result));
				break;

			// StatementExpression ;
			case ASTNode.EXPRESSION_STATEMENT:
				ExpressionStatement expStm = (ExpressionStatement) root;
				result.putAll(this.getReadDeclDefNodes(expStm.getExpression(),
						result));
				break;

			// Expression . Identifier
			case ASTNode.FIELD_ACCESS:
				FieldAccess fieldAccStm = (FieldAccess) root;
				result.putAll(this.getReadDeclDefNodes(
						fieldAccStm.getExpression(), result));
				result.putAll(this.getReadDeclDefNodes(fieldAccStm.getName(),
						result));
				break;

			// [Javadoc] { ExtendedModifier } Type VariableDeclarationFragment
			// { , VariableDeclarationFragment } ;
			case ASTNode.FIELD_DECLARATION:
				// System.err.println("Not implemented yet: " + root);
				break;

			// for (
			// [ ForInit ];
			// [ Expression ] ;
			// [ ForUpdate ] )
			// Statement
			// ForInit:
			// Expression { , Expression }
			// ForUpdate:
			// Expression { , Expression }
			case ASTNode.FOR_STATEMENT:
				ForStatement forStm = (ForStatement) root;

				for (Object astNode : forStm.initializers()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}

				for (Object astNode : forStm.updaters()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}

				result.putAll(this.getReadDeclDefNodes(forStm.getExpression(),
						result));
				// result.putAll(this.getReadDeclDefNodes(forStm.getBody(),
				// result));
				break;

			// if ( Expression ) Statement [ else Statement]
			case ASTNode.IF_STATEMENT:
				IfStatement ifStm = (IfStatement) root;
				result.putAll(this.getReadDeclDefNodes(ifStm.getExpression(),
						result));
				// result.putAll(this.getReadDeclDefNodes(ifStm.getThenStatement(),
				// result));
				// result.putAll(this.getReadDeclDefNodes(ifStm.getElseStatement(),
				// result));
				break;

			// Expression InfixOperator Expression { InfixOperator Expression }
			case ASTNode.INFIX_EXPRESSION:
				InfixExpression infixExp = (InfixExpression) root;
				result.putAll(this.getReadDeclDefNodes(
						infixExp.getLeftOperand(), result));
				result.putAll(this.getReadDeclDefNodes(
						infixExp.getRightOperand(), result));
				for (Object astNode : infixExp.extendedOperands()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// [ static ] Block
			case ASTNode.INITIALIZER:
				// System.err.println("not implemented yet: " + root);
				break;

			// Identfier : Statement
			case ASTNode.LABELED_STATEMENT:
				LabeledStatement labelStm = (LabeledStatement) root;
				result.putAll(this.getReadDeclDefNodes(labelStm.getBody(),
						result));
				break;

			// MethodDeclaration:
			// [ Javadoc ] { ExtendedModifier }
			// [ < TypeParameter { , TypeParameter } > ]
			// ( Type | void ) Identifier (
			// [ FormalParameter
			// { , FormalParameter } ] ) {[ ] }
			// [ throws TypeName { , TypeName } ] ( Block | ; )
			// ConstructorDeclaration:
			// [ Javadoc ] { ExtendedModifier }
			// [ < TypeParameter { , TypeParameter } > ]
			// Identifier (
			// [ FormalParameter
			// { , FormalParameter } ] )
			// [throws TypeName { , TypeName } ] Block
			case ASTNode.METHOD_DECLARATION:
				MethodDeclaration md = (MethodDeclaration) root;

				for (Object astNode : md.parameters()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// [ Expression . ]
			// [ < Type { , Type } > ]
			// Identifier ( [ Expression { , Expression } ] )
			case ASTNode.METHOD_INVOCATION:
				MethodInvocation method = (MethodInvocation) root;
				IMethodBinding met = method.resolveMethodBinding();

				for (Object astNode : method.arguments())
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));

				if (met != null && !Modifier.isStatic(met.getModifiers())) {
					result.putAll(this.getReadDeclDefNodes(
							(method.getExpression()), result));
				}
				break;

			// ( Expression )
			case ASTNode.PARENTHESIZED_EXPRESSION:
				ParenthesizedExpression pExp = (ParenthesizedExpression) root;
				result.putAll(this.getReadDeclDefNodes(pExp.getExpression(),
						result));
				break;

			// PrefixOperator Expression
			case ASTNode.PREFIX_EXPRESSION:
				PrefixExpression prefixExp = (PrefixExpression) root;
				result.putAll(this.getReadDeclDefNodes(prefixExp.getOperand(),
						result));
				break;

			// Name . SimpleName
			case ASTNode.QUALIFIED_NAME:
				QualifiedName qName = (QualifiedName) root;
				result.putAll(this.getReadDeclDefNodes(qName.getName(), result));
				break;

			// return [ Expression ] ;
			case ASTNode.RETURN_STATEMENT:
				ReturnStatement retStm = (ReturnStatement) root;
				result.putAll(this.getReadDeclDefNodes(retStm.getExpression(),
						result));
				break;

			// Identifier
			case ASTNode.SIMPLE_NAME:
				SimpleName name = (SimpleName) root;

				HashSet<ID_TYPE> idTypes = this.getNodeIdType(name);

				if (idTypes.contains(ID_TYPE.READ))
					read.add(name);
				if (idTypes.contains(ID_TYPE.DECLARATION))
					decl.add(name);
				if (idTypes.contains(ID_TYPE.DEFINITION))
					def.add(name);

				result.put(ID_TYPE.READ, read);
				result.put(ID_TYPE.DECLARATION, decl);
				result.put(ID_TYPE.DEFINITION, def);

				break;

			// { ExtendedModifier } Type [ ... ] Identifier { [] } [ =
			// Expression ]
			case ASTNode.SINGLE_VARIABLE_DECLARATION:
				SingleVariableDeclaration singleVar = (SingleVariableDeclaration) root;
				result.putAll(this.getReadDeclDefNodes(
						singleVar.getInitializer(), result));
				result.putAll(this.getReadDeclDefNodes(singleVar.getName(),
						result));
				break;

			// [ Expression . ]
			// [ < Type { , Type } > ]
			// super ( [ Expression { , Expression } ] ) ;
			case ASTNode.SUPER_CONSTRUCTOR_INVOCATION:
				SuperConstructorInvocation superInv = (SuperConstructorInvocation) root;
				result.putAll(this.getReadDeclDefNodes(
						superInv.getExpression(), result));
				for (Object node : superInv.arguments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) node,
							result));
				}
				break;

			// [ ClassName . ] super . Identifier
			case ASTNode.SUPER_FIELD_ACCESS:
				// System.err.println("not implemented yet: " + root);
				break;

			// [ ClassName . ] super .
			// [ < Type { , Type } > ]
			// Identifier ( [ Expression { , Expression } ] )
			case ASTNode.SUPER_METHOD_INVOCATION:
				SuperMethodInvocation superMetInv = (SuperMethodInvocation) root;
				for (Object node : superMetInv.arguments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) node,
							result));
				}
				break;

			// case Expression :
			// default :
			case ASTNode.SWITCH_CASE:
				SwitchCase caseStm = (SwitchCase) root;
				result.putAll(this.getReadDeclDefNodes(caseStm.getExpression(),
						result));
				break;

			// switch ( Expression )
			// { { SwitchCase | Statement } } }
			// SwitchCase:
			// case Expression :
			// default :
			case ASTNode.SWITCH_STATEMENT:
				SwitchStatement switchStm = (SwitchStatement) root;
				result.putAll(this.getReadDeclDefNodes(
						switchStm.getExpression(), result));
				break;

			// synchronized ( Expression ) Block
			case ASTNode.SYNCHRONIZED_STATEMENT:
				SynchronizedStatement syhncStm = (SynchronizedStatement) root;
				result.putAll(this.getReadDeclDefNodes(
						syhncStm.getExpression(), result));
				// result.putAll(this.getReadDeclDefNodes(syhncStm.getBody(),
				// result));
				break;

			// [ ClassName . ] this
			case ASTNode.THIS_EXPRESSION:
				// System.err.println("not implemented yet: " + root);
				break;

			// throw Expression ;
			case ASTNode.THROW_STATEMENT:
				ThrowStatement throwStm = (ThrowStatement) root;
				result.putAll(this.getReadDeclDefNodes(
						throwStm.getExpression(), result));
				break;

			// try Block
			// { CatchClause }
			// [ finally Block ]
			case ASTNode.TRY_STATEMENT:
				// TryStatement tryStm = (TryStatement) root;
				// result.putAll(this.getReadDeclDefNodes(tryStm.getBody(),
				// result));
				// result.putAll(this.getReadDeclDefNodes(tryStm.getFinally(),
				// result));

				// for (Object node : tryStm.catchClauses()) {
				// result.putAll(this.getReadDeclDefNodes((ASTNode) node,
				// result));
				// }

				break;

			// { ExtendedModifier } Type VariableDeclarationFragment
			// { , VariableDeclarationFragment }
			case ASTNode.VARIABLE_DECLARATION_EXPRESSION:
				VariableDeclarationExpression varDeclExp = (VariableDeclarationExpression) root;
				for (Object astNode : varDeclExp.fragments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// Identifier { [] } [ = Expression ]
			case ASTNode.VARIABLE_DECLARATION_FRAGMENT:
				VariableDeclarationFragment varDeclFrag = (VariableDeclarationFragment) root;
				result.putAll(this.getReadDeclDefNodes(
						varDeclFrag.getInitializer(), result));
				result.putAll(this.getReadDeclDefNodes(varDeclFrag.getName(),
						result));
				break;

			// { ExtendedModifier } Type VariableDeclarationFragment
			// { , VariableDeclarationFragment } ;
			case ASTNode.VARIABLE_DECLARATION_STATEMENT:
				VariableDeclarationStatement varDeclStm = (VariableDeclarationStatement) root;
				for (Object astNode : varDeclStm.fragments()) {
					result.putAll(this.getReadDeclDefNodes((ASTNode) astNode,
							result));
				}
				break;

			// while ( Expression ) Statement
			case ASTNode.WHILE_STATEMENT:
				WhileStatement whileStm = (WhileStatement) root;
				result.putAll(this.getReadDeclDefNodes(
						whileStm.getExpression(), result));
				// result.putAll(this.getReadDeclDefNodes(whileStm.getBody(),
				// result));
				break;

			// Expression instanceof Type
			case ASTNode.INSTANCEOF_EXPRESSION:
				InstanceofExpression instanceExp = (InstanceofExpression) root;
				result.putAll(this.getReadDeclDefNodes(
						instanceExp.getLeftOperand(), result));
				break;

			// for ( FormalParameter : Expression )
			// Statement
			case ASTNode.ENHANCED_FOR_STATEMENT:
				EnhancedForStatement enhForStm = (EnhancedForStatement) root;
				result.putAll(this.getReadDeclDefNodes(
						enhForStm.getExpression(), result));
				// result.putAll(this.getReadDeclDefNodes(enhForStm.getBody(),
				// result));
				break;
			}

		return result;
	}

	/**
	 * get the possibly types of one SimpleName node
	 * 
	 * @param name
	 *            the node
	 * @return a list with the types of node name
	 */
	private HashSet<ID_TYPE> getNodeIdType(SimpleName name) {

		HashSet<ID_TYPE> types = new HashSet<AnnotationVisitor.ID_TYPE>();

		if (name.isDeclaration()) {
			types.add(ID_TYPE.DECLARATION);

			VariableDeclarationFragment frag = (VariableDeclarationFragment) ASTNodes
					.getParent(name, ASTNode.VARIABLE_DECLARATION_FRAGMENT);
			SingleVariableDeclaration singleVar = (SingleVariableDeclaration) ASTNodes
					.getParent(name, ASTNode.SINGLE_VARIABLE_DECLARATION);

			Expression initializer = frag != null ? frag.getInitializer()
					: singleVar != null ? singleVar.getInitializer() : null;

			if (initializer != null)
				types.add(ID_TYPE.DEFINITION);

		} else {
			Assignment ass = (Assignment) ASTNodes.getParent(name,
					ASTNode.ASSIGNMENT);

			if (ass != null) {
				if (ass.getRightHandSide().toString().contains(name.toString()))
					types.add(ID_TYPE.READ);
				if (ass.getLeftHandSide().toString().contains(name.toString()))
					types.add(ID_TYPE.DEFINITION);
			}
		}

		if (types.size() == 0)
			types.add(ID_TYPE.READ);

		return types;
	}

	/**
	 * @param nodeSeed
	 * @return
	 */
	private Collection<? extends ASTNode> handleSwitchCase(ASTNode nodeSeed) {
		ArrayList<ASTNode> relatedStms = new ArrayList<ASTNode>();

		ASTNode switchNode = ASTNodes.getParent(nodeSeed,
				ASTNode.SWITCH_STATEMENT);
		ASTNode bufferCase = null;
		ASTNode bufferBreak = null;

		for (ASTNode stm : this.getSubStatements(switchNode)) {

			if (isOk(stm, ID_TYPE.READ, nodeSeed)) {
				if (stm.getNodeType() == ASTNode.SWITCH_CASE) {
					if (bufferCase == null)
						bufferCase = stm;
					else if (bufferCase.getStartPosition() < stm
							.getStartPosition())
						bufferCase = stm;
				}
			} else if (isOk(stm, ID_TYPE.DECLARATION, nodeSeed)) {
				if (stm.getNodeType() == ASTNode.BREAK_STATEMENT
						|| stm.getNodeType() == ASTNode.SWITCH_CASE) {
					if (bufferBreak == null)
						bufferBreak = stm;
					else if (bufferBreak.getStartPosition() > stm
							.getStartPosition())
						bufferBreak = stm;
				}
			}
		}

		relatedStms.add(bufferCase);
		if (bufferBreak != null
				&& bufferBreak.getNodeType() == ASTNode.BREAK_STATEMENT)
			relatedStms.add(bufferBreak);

		return relatedStms;
	}

	/**
	 * @return the examples
	 */
	public ArrayList<Example> getExamples() {
		return examples;
	}

}