package br.ufmg.dcc.llp.util.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public abstract class GenericXMLLoader <Target extends Object> {

    protected File file;
    
    public GenericXMLLoader() {
        file = null;
    }
    
    private void setFile(File file) throws FileNotFoundException {
        this.file = file;
        
        if(!file.exists() || !file.isFile())
            throw new FileNotFoundException();        
    }
    
    public GenericXMLLoader(File file) throws FileNotFoundException {
        this.setFile(file);
    }
    
    public GenericXMLLoader(String filePath) throws FileNotFoundException {
        this.setFile(new File(filePath));
    }
    
    protected Target load(Class c) {
        Target t = null;
        
        if (file == null)
            throw new RuntimeException ("Arquivo de saída deve ser informado!");
        
        try {
            JAXBContext jc = JAXBContext.newInstance(c);
            Unmarshaller un = jc.createUnmarshaller();
            InputStream is = new FileInputStream(file);
            t = (Target)un.unmarshal(is);
        } catch (JAXBException ex) {
            Logger.getLogger(GenericXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return t;        
    }
    
    protected Target load(Class c, String filePath) throws FileNotFoundException {
        this.setFile(new File(filePath));
        return load(c);
    }
    
    protected void store(Class c, Target t) {
        
        if (file == null)
            throw new RuntimeException ("Arquivo de saída deve ser informado!");
        
        try {
            JAXBContext jc = JAXBContext.newInstance(c);
            Marshaller m = jc.createMarshaller();
            OutputStream is = new FileOutputStream(file);
            m.marshal(t, is);
        } catch (JAXBException ex) {
            Logger.getLogger(GenericXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericXMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void store(Class c, Target t, String filePath) throws FileNotFoundException {              
        
        File f = new File(filePath);
        try {
            OutputStream os = new FileOutputStream(f);
            os.close();
        } catch(Exception e) { }
        
        this.setFile(new File(filePath));
        store(c, t);
    }
}
