package br.ufmg.dcc.llp.util.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
   
    private final static String dsn = "org.postgresql.Driver";
    private final static String dbURL = "jdbc:postgresql://java.llp.dcc.ufmg.br:5432/ovisadb";
    private final static String user = "ovisauser";
    private final static String pass = "kldc+748";
   
    private Connection dbConn;
    private static DBConnection dbConnection;
    private boolean driverLoaded = false;

    private DBConnection() { }

    public static DBConnection getInstance(){
        if (dbConnection == null) {
            dbConnection = new DBConnection();
        }
        return dbConnection;
    }

    public synchronized Connection getConnection() throws SQLException, ClassNotFoundException {
        if (!driverLoaded)
            loadDriver();

        if (dbConn == null) {
            openConnection();
        }
        return dbConn;
    }

    private void openConnection() throws SQLException {
        dbConn = DriverManager.getConnection(dbURL, user, pass);
        dbConn.setAutoCommit(true);
    }

    private void loadDriver() throws ClassNotFoundException {
        Class.forName(dsn);
        driverLoaded = true;
    }
    
    private void closeConnection() throws SQLException {
        if(dbConn != null)
            dbConn.close();
        
        dbConn = null;
    }
}
