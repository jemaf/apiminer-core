package br.ufmg.dcc.llp.util.persistence.jpa;

public interface IGenericEntity {
    public Long getPrimaryKey();
}
