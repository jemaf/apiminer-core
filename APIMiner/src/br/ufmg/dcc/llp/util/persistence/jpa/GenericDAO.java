package br.ufmg.dcc.llp.util.persistence.jpa;

import br.ufmg.dcc.llp.util.persistence.EMConnection;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

public abstract class GenericDAO <Entity extends IGenericEntity> {

    protected EntityManager em;
   
    public GenericDAO() {
        em = EMConnection.getEntityManager();
    }
    
    private Class<Entity> getEntityClass() {
        return (Class<Entity>) ((ParameterizedType)
                getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    protected void prePersist(Entity entity){ };
    
    public Entity adicionar(Entity entity) {
        prePersist(entity);
        em.getTransaction().begin();
        try {
        	if(entity.getPrimaryKey() == null)
        		em.persist(entity);
        	else
        		em.merge(entity);
            em.flush();
        }
        catch(PersistenceException pe) {
            em.getTransaction().rollback();
            throw pe;
        }
        
        em.getTransaction().commit();
        return em.getReference(this.getEntityClass(), entity.getPrimaryKey());        
    }
    
    public Entity atualizar(Entity entity) {
        prePersist(entity);
        em.getTransaction().begin();
        try {
            em.merge(entity);
            em.flush();
        }
        catch (PersistenceException pe) {
            em.getTransaction().rollback();
            throw pe;
        }

        em.getTransaction().commit();
        return em.getReference(this.getEntityClass(), entity.getPrimaryKey());        
    }

    public Entity remover(Entity entity) {
        Entity rt = em.getReference(this.getEntityClass(), entity.getPrimaryKey());
        em.getTransaction().begin();
        try {
            em.remove(rt);
        }
        catch (PersistenceException pe) {
            em.getTransaction().rollback();
            throw pe;
        }        
        em.getTransaction().commit();
        return rt;        
    }
           
    public Entity consultarPorId(Long entityId) {
        
        Class<Entity> entityClass = this.getEntityClass();
        
        Query qry = em.createNamedQuery(entityClass.getSimpleName() + ".findById", entityClass);
        qry.setParameter("id", entityId);
        
        if (qry.getResultList().isEmpty())
            return null;
               
        return (Entity)qry.getSingleResult();    
    }    

    public Entity consultarPorNome(String entityName) {
        
        Class<Entity> entityClass = this.getEntityClass();
        
        Query qry = em.createNamedQuery(entityClass.getSimpleName() + ".findByName", entityClass);
        qry.setParameter("name", entityName);
        
        if (qry.getResultList().isEmpty())
            return null;
               
        return (Entity)qry.getSingleResult();    
    }
    
    public List<Entity> consultarTodos() {

        Query qry = em.createNamedQuery(this.getEntityClass().getSimpleName() + ".findAll");
               
        return qry.getResultList();
    }
   
}
