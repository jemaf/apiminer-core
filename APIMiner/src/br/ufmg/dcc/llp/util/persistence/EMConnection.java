package br.ufmg.dcc.llp.util.persistence;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMConnection {
   
    public static final String PUSERVER = "apiminerPU";
    
    private final static String persistenceUnit = PUSERVER;
    
    private static Map<String, EntityManagerFactory> emfTable = new HashMap<String, EntityManagerFactory>();
    
    public static EntityManager getEntityManager() {

        return getEntityManager(persistenceUnit);
    }
    
    public static EntityManager getEntityManager(String pu) {

        EntityManagerFactory emf = emfTable.get(pu);
        
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(pu);            
            emfTable.put(pu, emf);
        }
        
        return emf.createEntityManager();        
    }
    
}
